package org.jeefb.common.persistence;

import java.io.Serializable;
import java.util.Date;
/**
 * 
 * @ClassName: BaseEntity 
 * @Description: 实体抽象类
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 下午1:21:40 
 *
 */
public abstract class BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date updateDate;// 更新日期

	private String updateBy;// 更新人ID

	private Date createDate;// 创建日期

	private String createBy;// 创建人ID


	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}
