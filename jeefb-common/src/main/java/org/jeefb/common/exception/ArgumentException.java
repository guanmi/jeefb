package org.jeefb.common.exception;

/**
 * 
 * @ClassName: ArgumentException 
 * @Description: 参数错误，方法调用的入参不符合要求 
 * @author genemax1107@aliyun.com
 * @date 2016年5月20日 下午3:44:31 
 *
 */
public class ArgumentException extends BusinessException {
    private static final long serialVersionUID = 6832695224568830049L;

    public ArgumentException(String message) {
        super(message);
    }
}