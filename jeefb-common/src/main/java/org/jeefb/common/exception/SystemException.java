package org.jeefb.common.exception;

/**
 * 
 * @ClassName: SystemException 
 * @Description: 系统异常，例如数据库错误、网络错误等
 * @author genemax1107@aliyun.com
 * @date 2016年5月20日 下午3:45:29 
 *
 */
public class SystemException extends Exception {
    private static final long serialVersionUID = -8649925931214974641L;

    public SystemException() {
        super();
    }

    public SystemException(String message) {
        super(message);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }
}