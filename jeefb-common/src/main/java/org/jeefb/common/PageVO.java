package org.jeefb.common;

import java.io.Serializable;
import java.util.List;
/**
 * 
 * @ClassName: PageVO 
 * @Description: 分页对象 
 * @author genemax1107@aliyun.com
 * @param <T>
 * @date 2016年6月19日 上午11:58:45 
 *
 */
public class  PageVO<T>  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private  Integer offset;//起启记录数
	private  String order;
	private Long total;
	private List<T> rows;
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	
	

}
