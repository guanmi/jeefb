 package org.jeefb.common.utils;
 
 import java.io.Serializable;
 import java.util.Date;
 
 public class DatePaire
   implements Serializable
 {
   private static final long serialVersionUID = -5460354342272327866L;
   private Date start;
   private Date end;
 
   public Date getStart()
   {
     return this.start;
   }
   public void setStart(Date start) {
     this.start = start;
   }
   public Date getEnd() {
     return this.end;
   }
   public void setEnd(Date end) {
     this.end = end;
   }
 }