package org.jeefb.common.utils;
import java.security.MessageDigest;
/**
 * 
 * @ClassName: EncryptUtils 
 * @Description: 用户密码加密
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 下午12:04:11 
 *
 */
public class EncryptUtils {
 
 private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5",
   "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
 
 private Object salt;
 private String algorithm;
 public static final String MD5="MD5";
 public static final String SHA="SHA";
 
 public EncryptUtils(Object salt, String algorithm) {
  this.salt = salt;
  this.algorithm = algorithm;
 }

 public String encode(String rawPass) {
  String result = null;
  try {
			MessageDigest md = MessageDigest.getInstance(algorithm);
   //加密后的字符串  
   result = byteArrayToHexString(md.digest(mergePasswordAndSalt(rawPass).getBytes("utf-8")));
  } catch (Exception ex) {
  }
  return result;
 }
 
    
    private String mergePasswordAndSalt(String password) {
        if (password == null) {
            password = "";
        }

        if ((salt == null) || "".equals(salt)) {
            return password;
        } else {
            return password + "{" + salt.toString() + "}";
        }
    }

 /**
  * 转换字节数组为16进制字串
  * @param b 字节数组
  * @return 16进制字串
  */
 private static String byteArrayToHexString(byte[] b) {
  StringBuffer resultSb = new StringBuffer();
  for (int i = 0; i < b.length; i++) {
   resultSb.append(byteToHexString(b[i]));
  }
  return resultSb.toString();
 }

 private static String byteToHexString(byte b) {
  int n = b;
  if (n < 0)
   n = 256 + n;
  int d1 = n / 16;
  int d2 = n % 16;
  return hexDigits[d1] + hexDigits[d2];
 }
 /**
  * 
  * @Description: 加密
  * @param @param salt
  * @param @param password
  * @param @return    
  * @return String
  */
 public static String   enCode(String salt,String password){
	 EncryptUtils encoderMd5 = new EncryptUtils(salt, MD5);
     String encode = encoderMd5.encode(password);
     return encode;
 }
 /**
  * 
  * @Description: 比较密码
  * @param @param salt
  * @param @param password
  * @param @param dbPasswrod
  * @param @return    
  * @return boolean
  */
 public static boolean   desCode(String salt,String password,String dbPasswrod){
	String encode = enCode(salt, password);
	return dbPasswrod.equals(encode);
 }
 


}