 package org.jeefb.common.utils;
 
 import com.alibaba.fastjson.JSON;
 import com.alibaba.fastjson.TypeReference;
 import com.alibaba.fastjson.parser.Feature;
 import com.alibaba.fastjson.serializer.SerializeConfig;
 import com.alibaba.fastjson.serializer.SerializerFeature;
 import java.lang.reflect.Array;
 import java.lang.reflect.Field;
 import java.lang.reflect.Method;
 import java.util.ArrayList;
 import java.util.Iterator;
 import java.util.List;
 import java.util.Map;
 import org.apache.commons.lang3.StringEscapeUtils;
 import org.apache.commons.lang3.StringUtils;
 
 public class JsonUtils
 {
   private static final SerializeConfig config = new SerializeConfig();
 
   private static final SerializerFeature[] features = { SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullListAsEmpty, SerializerFeature.WriteNullNumberAsZero, SerializerFeature.WriteNullBooleanAsFalse, SerializerFeature.WriteNullStringAsEmpty };
 
   public static String toJson(Object obj)
   {
     return JSON.toJSONString(obj, config, features);
   }
 
   public static <T> T toBean(String json, Class<T> clazz)
   {
     try
     {
       json = StringEscapeUtils.unescapeHtml4(json);
       return JSON.parseObject(json, clazz);
     }
     catch (Exception e) {
       throw new RuntimeException(new StringBuilder().append("json toBean:").append(clazz).append("  error,json:").append(json).toString(), e);
     }
   }
 
   public static <T> List<T> toList(String json, Class<T> clazz)
   {
     json = StringEscapeUtils.unescapeHtml4(json);
     List lt = JSON.parseArray(json, clazz);
     if (lt == null) {
       lt = new ArrayList(0);
     }
 
     Iterator iter = lt.iterator();
     while (iter.hasNext()) {
       if (iter.next() == null) {
         iter.remove();
       }
     }
     return lt;
   }
 
   public static <K, V> Map<K, V> toMap(String json)
   {
     json = StringEscapeUtils.unescapeHtml4(json);
     return (Map)JSON.parseObject(json, new TypeReference()
     {
     }
     , new Feature[0]);
   }
 
   public static String arrayToJson(List<?> list)
   {
     StringBuilder builder = new StringBuilder();
     builder.append("[");
     int entryIndex = 0;
     for (Iterator i$ = list.iterator(); i$.hasNext(); ) { Object obj = i$.next();
       if (entryIndex != 0) {
         builder.append(",");
       }
       builder.append(toJson(obj));
       entryIndex++;
     }
     builder.append("]");
     return builder.toString();
   }
 
   public static String complexObjectToJson(Object object) {
     StringBuffer b = new StringBuffer();
     b.append("{");
     Class clazz = object.getClass();
     Field[] fields = clazz.getDeclaredFields();
     int fieldIndex = 0;
     for (Field f : fields) {
       String fname = f.getName();
       String mname = new StringBuilder().append("get").append(fname.substring(0, 1).toUpperCase()).append(fname.substring(1, fname.length())).toString();
       try
       {
         Method m = object.getClass().getMethod(mname, new Class[0]);
         Object data = m.invoke(object, new Object[0]);
         if ((data != null) && (StringUtils.isNotBlank(data.toString()))) {
           if (fieldIndex != 0) {
             b.append(",");
           }
           fieldIndex++;
           b.append(new StringBuilder().append("\"").append(fname).append("\"").toString());
           b.append(":");
         } else {
           continue;
         }
 
         Class returnType = m.getReturnType();
         if ((returnType == List.class) || (returnType == Array.class)) {
           b.append("[");
           int entryIndex = 0;
           List list = (List)m.invoke(object, new Object[0]);
           if ((list == null) || (list.size() == 0)) {
             b.append("]");
           }
           else {
             for (Iterator i$ = list.iterator(); i$.hasNext(); ) { Object obj = i$.next();
               if (entryIndex != 0) {
                 b.append(",");
               }
               b.append(complexObjectToJson(obj));
               entryIndex++;
             }
             b.append("]");
           }
         } else { b.append(m.invoke(object, new Object[0])); }
       }
       catch (Exception e)
       {
         e.printStackTrace();
       }
     }
     b.append("}");
     return b.toString();
   }
 }