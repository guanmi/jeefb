package org.jeefb.common.utils;

import java.util.Date;

import com.alibaba.fastjson.JSON;

/**
 * 
 * @ClassName: ExceptionUtils
 * @Description: 异常帮助类
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 下午1:30:21
 *
 */
public class ExceptionUtils {
	/**
	 * 
	 * @Description: 打印异常信息
	 * @param @param
	 *            ex
	 * @param @return
	 * @return String
	 */
	public static String printExceptionInfo(Exception ex, Object... objs) {
		StackTraceElement[] st = ex.getStackTrace();
		StringBuffer sb = new StringBuffer();
		int i = 0;
		for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			sb.append(new Date() + ":" + "[类:" + exclass + "]调用" + method + "时在第" + stackTraceElement.getLineNumber()
					+ "行代码处发生异常!异常类型:" + ex.getClass().getName()+"\n");
			i++;
			if(i>2){
				break;
			}
		}
		sb.append(" 参数：");
		for (Object object : objs) {
			sb.append(JSON.toJSONString(object));
		}
		return sb.toString();
	}
}
