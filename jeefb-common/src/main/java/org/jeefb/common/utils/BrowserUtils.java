package org.jeefb.common.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @ClassName: BrowserUtils 
 * @Description: 浏览器多语言工具类
 * @author genemax1107@aliyun.com
 * @date 2016年6月14日 下午10:21:17 
 *
 */
public class BrowserUtils {
	
	private static Map<String, String> langMap = new HashMap<String, String>();
	private final static String ZH = "zh";
	private final static String ZH_CN = "zh_CN";
	
	private final static String EN = "en";
	private final static String EN_US = "en_US";
	
	
	static 
	{
		langMap.put(ZH, ZH_CN);
		langMap.put(EN, EN_US);
	}
	
	public static String getBrowserLanguage(HttpServletRequest request) {
		String browserLang = request.getLocale().getLanguage();
		String browserLangCode = (String)langMap.get(browserLang);
		if(browserLangCode == null)
		{
			browserLangCode = EN_US;
		}
		return browserLangCode;
	}

}
