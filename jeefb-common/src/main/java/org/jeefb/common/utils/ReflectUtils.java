package org.jeefb.common.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

@SuppressWarnings({"unchecked","rawtypes"})
public class ReflectUtils {
    /**
     * 调用类的方法，可进行动态传参
     * @param clazz
     * @param methodName
     * @param objs
     * @return
     */
	public static Object invokeByClassAndMethodName( Class clazz,String methodName,Object...objs){
	        try {
	        	
	        	  Class c[]=null;  
	              if(objs!=null){
	                  int len=objs.length; 
	                  c=new Class[len];  
	                  for(int i=0;i<len;++i){  
	                      c[i]=objs[i].getClass(); 
	                  } 
	              } 
	           
				Method method = clazz.getMethod(methodName,c);
	            return method.invoke(clazz,objs);
	        } catch (NoSuchMethodException e) {
	            e.printStackTrace();
	        } catch (SecurityException e) {
	            e.printStackTrace();
	        } catch (IllegalAccessException e) {
	            e.printStackTrace();
	        } catch (IllegalArgumentException e) {
	            e.printStackTrace();
	        } catch (InvocationTargetException e) {
	            e.printStackTrace();
	        }
	        return null;
	    }
        /**
         * 根据class名称生成实例
         * @param className
         * @return
         */
      public static Object getClassInstanceByName(String className){
          Object clazz =  null;
        try {
            Class targetClass = Class.forName(className);
            clazz = targetClass.newInstance();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return clazz;         
      }
	/**
	 * 根据属性名获取属性值
	 * */
    public static  Object getFieldValueByName(String fieldName, Object o) {
        try {  
            String firstLetter = fieldName.substring(0, 1).toUpperCase();  
            String getter = "get" + firstLetter + fieldName.substring(1);  
            Method method = o.getClass().getMethod(getter, new Class[] {});  
            Object value = method.invoke(o, new Object[] {});  
            return value;  
        } catch (Exception e) {  
            return null;  
        }  
    } 
	

	
	/**
	 * 根据属性名，属性值设置数据值
	 * @param entity
	 * @param filedName
	 * @param filedValue
	 * @return
	 */
	public  static Object setFieldValueByPropertyName(Object entity,String filedName,Object filedValue){
		try {
			Field field = entity.getClass().getDeclaredField(filedName);
			field.setAccessible(true);
			if (field.getType().equals(Date.class)) {
				if (filedValue!=null) {
					field.set(entity, DateUtils.getDateString(String.valueOf(filedValue)));// 动态设置值
				} else {
					field.set(entity, null);// 动态设置值
				}
			} 
			if (field.getType().equals(Double.class)) {
				field.set(entity, Double.parseDouble(String.valueOf(filedValue)));
			} 
			if (field.getType().equals(Long.class)) {
				field.set(entity, Long.parseLong(String.valueOf(filedValue)));
			}
			if (field.getType().equals(String.class)) {
				field.set(entity, filedValue);// 动态设置值
			}
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} 
		return entity;
	}
	
	
	
}
