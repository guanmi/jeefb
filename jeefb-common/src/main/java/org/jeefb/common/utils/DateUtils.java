 package org.jeefb.common.utils;
 
 import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
 
 public class DateUtils
 {
   public static final String DEFAULT_TIMEZONE = "GMT+0";
   public static final String DATEFORMAT_YYYYMMDD = "yyyyMMdd";
   public static final String DATEFORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
   public static final String DATEFORMAT_YYYY_MM_DD = "yyyy-MM-dd";
   public static final String DATEFORMAT_YYYY_MM_DD_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
   public static final String DATEFORMAT_YYYY_MM_DD_HH_mm = "yyyy-MM-dd HH:mm";
   public static final String[] months = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "EPT", "OCT", "NOV", "DEC" };
   public static final String DATEFORMAT_YYYYMMDDHH = "yyyyMMddHH";
 
   public static Date getDate()
   {
     return new Date();
   }
 
   public static Calendar getCalendar()
   {
     return new GregorianCalendar();
   }
 
   public static Calendar getCalendar(Date date)
   {
     Calendar calendar = getCalendar();
     calendar.setTime(date);
     return calendar;
   }
 
   public static DateFormat getDateFormat(String pattern)
   {
     return new SimpleDateFormat(pattern, Locale.US);
   }
 
   public static Date clearTime(Date date)
   {
     return parseDate(dateToString(date, "yyyyMMdd"), "yyyyMMdd");
   }
 
   public static boolean between(Date start, Date end, Date date)
   {
     if ((start.getTime() <= date.getTime()) && (end.getTime() >= date.getTime()))
     {
       return true;
     }
     return false;
   }
 
   public static boolean between(Date start, Date end, Date date, String pattern)
   {
     start = toDate(start, pattern);
     end = toDate(end, pattern);
     date = toDate(date, pattern);
     if ((start.getTime() <= date.getTime()) && (end.getTime() >= date.getTime()))
     {
       return true;
     }
     return false;
   }
 
   public static boolean betweenIn24H(Date start, Date end, Date date)
   {
     if (start.equals(end)) {
       return true;
     }
     if (compareTime(date, start) < 0) {
       String dateStr = formatDate(addDay(date, -1), "yyyyMMdd");
       String startTimeStr = formatDate(start, "HHmmss");
       start = parseDate(dateStr + startTimeStr, "yyyyMMddHHmmss");
 
       String endTimeStr = dateToString(end, "HHmmss");
       end = parseDate(dateStr + endTimeStr, "yyyyMMddHHmmss");
       if (endTimeStr.compareTo(startTimeStr) < 0)
         end = addDay(end, 1);
     }
     else {
       String dateStr = formatDate(date, "yyyyMMdd");
       String startTimeStr = formatDate(start, "HHmmss");
       start = parseDate(dateStr + startTimeStr, "yyyyMMddHHmmss");
 
       String endTimeStr = dateToString(end, "HHmmss");
       end = parseDate(dateStr + endTimeStr, "yyyyMMddHHmmss");
       if (endTimeStr.compareTo(startTimeStr) < 0) {
         end = addDay(end, 1);
       }
     }
     return between(start, end, date);
   }
 
   public static int nearDate(Date start, Date end, Date date)
   {
     int s1 = subBySecond(start, date);
     int s2 = subBySecond(date, end);
     if (s1 >= s2) {
       return 2;
     }
     return 1;
   }
 
   public static int nearIn24H(Date start, Date end, Date date)
   {
     if (!betweenIn24H(start, end, date)) {
       throw new RuntimeException("时间不在时间段内 start:" + dateToString(start, "HH:mm:ss") + "end:" + dateToString(end, "HH:mm:ss") + "date:" + dateToString(date, "HH:mm:ss"));
     }
 
     if (compareTime(date, start) < 0) {
       String dateStr = formatDate(addDay(date, -1), "yyyyMMdd");
       String startTimeStr = formatDate(start, "HHmmss");
       start = parseDate(dateStr + startTimeStr, "yyyyMMddHHmmss");
 
       String endTimeStr = dateToString(end, "HHmmss");
       end = parseDate(dateStr + endTimeStr, "yyyyMMddHHmmss");
       if (endTimeStr.compareTo(startTimeStr) < 0)
         end = addDay(end, 1);
     }
     else {
       String dateStr = formatDate(date, "yyyyMMdd");
       String startTimeStr = formatDate(start, "HHmmss");
       start = parseDate(dateStr + startTimeStr, "yyyyMMddHHmmss");
 
       String endTimeStr = dateToString(end, "HHmmss");
       end = parseDate(dateStr + endTimeStr, "yyyyMMddHHmmss");
       if (endTimeStr.compareTo(startTimeStr) < 0) {
         end = addDay(end, 1);
       }
     }
     return nearDate(start, end, date);
   }
 
   public static boolean between(Date start1, Date end1, Date start2, Date end2)
   {
     Date f = future();
     if (end1 == null) {
       end1 = f;
     }
     if (end2 == null) {
       end2 = f;
     }
 
     if ((start1.compareTo(start2) <= 0) && (start2.compareTo(end1) <= 0)) {
       return true;
     }
     if ((start1.compareTo(end2) <= 0) && (end2.compareTo(end1) <= 0)) {
       return true;
     }
 
     if ((start2.compareTo(start1) <= 0) && (end2.compareTo(end1) >= 0)) {
       return true;
     }
 
     if (f.equals(end1)) {
       end1 = null;
     }
     if (f.equals(end2)) {
       end2 = null;
     }
     return false;
   }
 
   public static boolean contain(Date start1, Date end1, Date start2, Date end2)
   {
     if (start2 == null) {
       if (end2 == null) {
         return true;
       }
       return false;
     }
 
     if (start1 == null) {
       return false;
     }
 
     if (end1 == null) {
       if (end2 != null) {
         return false;
       }
       return start1.compareTo(start2) >= 0;
     }
     if (end1.compareTo(start2) == 0) {
       return start1.compareTo(start2) == 0;
     }
 
     if (end1.compareTo(start2) < 0) {
       return false;
     }
 
     if (end2 == null) {
       return start1.compareTo(start2) >= 0;
     }
 
     boolean flag1 = start1.compareTo(start2) >= 0;
     boolean flag2 = end1.compareTo(end2) <= 0;
     return (flag1) && (flag2);
   }
 
   public static String getNativeSQLDate(Date date)
   {
     String dbString = "to_date('%s','yyyy-mm-dd hh24:mi:ss')";
     String d = formatDate(date, "yyyy-MM-dd HH:mm:ss");
     return String.format(dbString, new Object[] { d });
   }
 
   public static boolean contain(Date compareDate, Date fromDate, Date toDate)
   {
     if (compareDate == null) {
       return false;
     }
     if (fromDate == null) {
       return false;
     }
     long date = compareDate.getTime();
     long from = fromDate.getTime();
     if (from <= date) {
       if (toDate != null) {
         long to = toDate.getTime();
         if (date <= to)
           return true;
       }
       else {
         return true;
       }
     }
     return false;
   }
 
   public static boolean timeCompare(Date start1, Date end1, Date start2, Date end2)
   {
     if ((end1 == null) && (end2 == null)) {
       return true;
     }
 
     int formAndTo = compareDateByDay(start2, end2);
     if (formAndTo > 0) {
       return false;
     }
 
     int from = compareDateByDay(start1, start2);
 
     if (from > 0) {
       return false;
     }
 
     int to = compareDateByDay(end1, end2);
 
     if (to < 1) {
       return false;
     }
 
     return false;
   }
 
   public static int compareDateByDay(Date date1, Date date2)
   {
     if (date1 == null) {
       date1 = future();
     }
     if (date2 == null) {
       date2 = future();
     }
     String d1 = dateToString(date1, "yyyyMMdd");
     String d2 = dateToString(date2, "yyyyMMdd");
     return d1.compareTo(d2);
   }
 
   public static int compareDateByTimestamp(Date date1, Date date2)
   {
     if (date1 == null) {
       date1 = future();
     }
     if (date2 == null) {
       date2 = future();
     }
     String d1 = dateToString(date1, "yyyyMMddHHmmss");
     String d2 = dateToString(date2, "yyyyMMddHHmmss");
     return d1.compareTo(d2);
   }
 
   public static int compareDateByMonth(Date date1, Date date2)
   {
     if (date1 == null) {
       date1 = future();
     }
     if (date2 == null) {
       date2 = future();
     }
     String d1 = dateToString(date1, "yyyyMM");
     String d2 = dateToString(date2, "yyyyMM");
     return d1.compareTo(d2);
   }
 
   public static int compareDateByPattern(Date date1, Date date2, String pattern)
   {
     if (date1 == null) {
       date1 = future();
     }
     if (date2 == null) {
       date2 = future();
     }
     String d1 = dateToString(date1, pattern);
     String d2 = dateToString(date2, pattern);
     return d1.compareTo(d2);
   }
 
   public static int compareDateByYear(Date date1, Date date2) {
     if (date1 == null) {
       date1 = future();
     }
     if (date2 == null) {
       date2 = future();
     }
     String d1 = dateToString(date1, "yyyy");
     String d2 = dateToString(date2, "yyyy");
     return d1.compareTo(d2);
   }
 
   public static String dateToString(Date date, String pattern)
   {
     return getDateFormat(pattern).format(date);
   }
 
   public static String dateToString(Date date)
   {
     return getDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
   }
 
   public static String formatDate(Date date, String pattern)
   {
     String result = null;
     if ((date == null) || (pattern == null))
       return result;
     try
     {
       result = getDateFormat(pattern).format(date);
     } catch (Exception e) {
       result = null;
     }
 
     return result;
   }
 
   public static Date future()
   {
     Date date = null;
     try {
       DateFormat df = getDateFormat("yyyyMMdd");
       date = df.parse(df.format(df.parse("22220202")));
     } catch (Exception e) {
       return null;
     }
     return date;
   }
 
   public static String getCurrQuarter()
   {
     try
     {
       Calendar calendar = getCalendar();
       DateFormat df = getDateFormat("yyyy-MM-dd hh:mm");
       String year = String.valueOf(calendar.get(1));
       Date dt = getDate();
 
       Date dt1 = df.parse(year + "-01-01 00:00");
       Date dt2 = df.parse(year + "-04-01 00:00");
       if ((dt.getTime() >= dt1.getTime()) && (dt.getTime() < dt2.getTime())) {
         return "01";
       }
       dt1 = df.parse(year + "-04-01 00:00");
       dt2 = df.parse(year + "-07-01 00:00");
       if ((dt.getTime() >= dt1.getTime()) && (dt.getTime() < dt2.getTime())) {
         return "02";
       }
       dt1 = df.parse(year + "-07-01 00:00");
       dt2 = df.parse(year + "-10-01 00:00");
       if ((dt.getTime() >= dt1.getTime()) && (dt.getTime() < dt2.getTime())) {
         return "03";
       }
       dt1 = df.parse(year + "-10-01 00:00");
       year = String.valueOf(calendar.get(1) + 1);
       dt2 = df.parse(year + "-01-01 00:00");
       if ((dt.getTime() >= dt1.getTime()) && (dt.getTime() < dt2.getTime()))
         return "04";
     }
     catch (Exception e) {
     }
     return null;
   }
 
   public static int getLastDay(Date date)
   {
     if (date == null) {
       return -1;
     }
     Calendar calendar = getCalendar(date);
     calendar.set(5, 1);
     calendar.roll(5, -1);
     return calendar.get(5);
   }
 
   public static long getDayTime(Date date)
   {
     return parseDate(dateToString(date, "yyMMdd"), "yyMMdd").getTime();
   }
 
   public static String getLastDayByMonth(String month)
   {
     if (null == month) {
       return null;
     }
     String year = month.substring(0, 4);
     String mon = month.substring(4, 6);
     String day = "30";
     List month30 = Arrays.asList(new String[] { "04", "06", "09", "11" });
     List month31 = Arrays.asList(new String[] { "01", "03", "05", "07", "08", "10", "12" });
 
     if (month30.contains(mon))
       day = "30";
     else if (month31.contains(mon)) {
       day = "31";
     }
     if (mon.equals("02")) {
       if (isLeapYear(Integer.valueOf(year).intValue()))
         day = "29";
       else {
         day = "28";
       }
     }
     return month + day;
   }
 
   public static boolean isLeapYear(int year)
   {
     if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
       return true;
     }
     return false;
   }
 
   public static String getLastDayByQuarter(String quarter)
   {
     if (null == quarter) {
       return null;
     }
     String year = quarter.substring(0, 4);
     String quart = quarter.substring(4, 6);
     String daily = "";
     if (quart.equals("01"))
       daily = "0331";
     else if (quart.equals("02"))
       daily = "0630";
     else if (quart.equals("03"))
       daily = "0930";
     else if (quart.equals("04"))
       daily = "1231";
     else {
       return null;
     }
     return year + daily;
   }
 
   public static String getQuarterLastMonth(String quarter)
   {
     if (null == quarter) {
       return null;
     }
     String year = quarter.substring(0, 4);
     String quart = quarter.substring(4, 6);
     String month = "";
     if (quart.equals("01"))
       month = "03";
     else if (quart.equals("02"))
       month = "06";
     else if (quart.equals("03"))
       month = "09";
     else if (quart.equals("04"))
       month = "12";
     else {
       return null;
     }
     return year + month;
   }
 
   public static String getNextQuarter(String curQuarter)
   {
     if (curQuarter.length() != 6) {
       return "";
     }
     int year = Integer.parseInt(curQuarter.substring(0, 4));
     int quart = Integer.parseInt(curQuarter.substring(4, 6));
     if (quart == 4) {
       year++;
       quart = 1;
     } else {
       quart++;
     }
     return year + "0" + quart;
   }
 
   public static Date addDay(Date date, int days)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(5, days);
     return c.getTime();
   }
 
   public static Date setDay(Date date, int day)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.set(5, day);
     return c.getTime();
   }
 
   public static Date setHour(Date date, int hour)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.set(11, hour);
     return c.getTime();
   }
 
   public static Date setMinute(Date date, int minute)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.set(12, minute);
     return c.getTime();
   }
 
   public static Date addHour(Date date, int hours)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(10, hours);
     return c.getTime();
   }
 
   public static Date addMinute(Date date, int minutes)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(12, minutes);
     return c.getTime();
   }
 
   public static Date addMonth(Date date, int months)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(2, months);
     return c.getTime();
   }
 
   public static Date addSecond(Date date, int seconds)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(13, seconds);
     return c.getTime();
   }
 
   public static Date addMilliSecond(Date date, int milliSeconds) {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(14, milliSeconds);
     return c.getTime();
   }
 
   public static Date addWeek(Date date, int weeks)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(4, weeks);
     return c.getTime();
   }
 
   public static Date addYear(Date date, int years)
   {
     if (date == null) {
       return null;
     }
     Calendar c = getCalendar(date);
     c.add(1, years);
     return c.getTime();
   }
 
   public static Date parseDate(String dateString, String format, TimeZone zone)
   {
     Date date = null;
     try {
       DateFormat df = getDateFormat(format);
       df.setTimeZone(zone);
       date = df.parse(dateString);
     } catch (Exception e) {
       return null;
     }
     return date;
   }
 
   public static Date parseDate(String dateString, String pattern)
   {
     Date result = null;
     DateFormat sdf = getDateFormat(pattern);
     sdf.setLenient(false);
     try {
       result = sdf.parse(dateString);
     } catch (Exception e) {
       e.printStackTrace();
       result = null;
     }
     return result;
   }
 
   public static Date parseDate(String dateString)
   {
     return parseDate(dateString, "yyyy-MM-dd HH:mm:ss");
   }
 
   public static Date getEndTimeOfDay(Date date)
   {
     Date result = null;
 
     if (date != null) {
       Calendar calendar = getCalendar(date);
       int year = calendar.get(1);
       int month = calendar.get(2);
       int day = calendar.get(5);
       calendar.set(year, month, day, 23, 59, 59);
       calendar.set(14, 999);
       result = calendar.getTime();
     }
     return result;
   }
 
   public static Date getStartTimeOfDay(Date date)
   {
     Date result = null;
     if (date != null) {
       Calendar calendar = getCalendar(date);
       int year = calendar.get(1);
       int month = calendar.get(2);
       int day = calendar.get(5);
       calendar.set(year, month, day, 0, 0, 0);
       calendar.set(14, 0);
       result = calendar.getTime();
     }
     return result;
   }
 
   public static Date today()
   {
     return today("yyyyMMdd");
   }
 
   public static Date today(String pattern)
   {
     Date date = getDate();
     if (pattern == null)
       return date;
     try
     {
       DateFormat df = getDateFormat(pattern);
       date = df.parse(df.format(getDate()));
     } catch (Exception e) {
       return date;
     }
     return date;
   }
 
   public static Date addDate(Date date, String unit, int quantity)
   {
     if (unit.equalsIgnoreCase("Year")) {
       return addYear(date, quantity);
     }
     if (unit.equalsIgnoreCase("Month")) {
       return addMonth(date, quantity);
     }
     if (unit.equalsIgnoreCase("Week")) {
       return addWeek(date, quantity);
     }
     if (unit.equalsIgnoreCase("Day")) {
       return addDay(date, quantity);
     }
     if (unit.equalsIgnoreCase("Hour")) {
       return addHour(date, quantity);
     }
     if (unit.equalsIgnoreCase("Minute")) {
       return addMinute(date, quantity);
     }
     if (unit.equalsIgnoreCase("Second")) {
       return addSecond(date, quantity);
     }
     return null;
   }
 
   public static int getHour(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(11);
   }
 
   public static int getMunite(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(12);
   }
 
   public static int getSecond(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(13);
   }
 
   public static int getMonth(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(2) + 1;
   }
 
   public static int getYear(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(1);
   }
 
   public static int getSeason(Date date)
   {
     int currentMonth = getMonth(date);
     if ((currentMonth >= 1) && (currentMonth <= 3))
       return 1;
     if ((currentMonth >= 4) && (currentMonth <= 6))
       return 2;
     if ((currentMonth >= 7) && (currentMonth <= 9))
       return 3;
     if ((currentMonth >= 10) && (currentMonth <= 12)) {
       return 4;
     }
     return 0;
   }
 
   public static int getDayOfMonth(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(5);
   }
 
   public static int getWeekOfYear(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(3);
   }
 
   public static int getDayOfWeek(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     return calendar.get(7) - 1 == 0 ? 7 : calendar.get(7) - 1;
   }
 
   public static String getDayStrOfWeek(Date date)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
 
     int day = calendar.get(7) - 1 == 0 ? 7 : calendar.get(7) - 1;
 
     String returnValue = "";
     if (day == 1)
     {
       returnValue = "星期一";
     } else if (day == 2)
     {
       returnValue = "星期二";
     }
     else if (day == 3)
     {
       returnValue = "星期三";
     }
     else if (day == 4)
     {
       returnValue = "星期四";
     }
     else if (day == 5)
     {
       returnValue = "星期五";
     }
     else if (day == 6)
     {
       returnValue = "星期六";
     }
     else if (day == 7)
     {
       returnValue = "星期日";
     }
 
     return returnValue;
   }
 
   public static DatePaire getDatePaire(Date date, int type)
   {
     DatePaire paire = new DatePaire();
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
     int year = calendar.get(1);
     int month = calendar.get(2);
     int day = calendar.get(5);
     Date start = null;
     Date end = null;
     if (type == 1) {
       Calendar startCal = Calendar.getInstance();
       startCal.set(year, 0, 1, 0, 0, 0);
       start = startCal.getTime();
 
       Calendar endCal = Calendar.getInstance();
       endCal.set(1, year);
       endCal.set(2, 11);
       int lastDay = endCal.getActualMaximum(5);
       endCal.set(year, 11, lastDay, 23, 59, 59);
       end = endCal.getTime();
     }
 
     if (type == 2) {
       Calendar startCal = Calendar.getInstance();
       startCal.set(year, month, 1, 0, 0, 0);
       start = startCal.getTime();
 
       int lastDay = startCal.getActualMaximum(5);
       startCal.set(year, month, lastDay, 23, 59, 59);
       end = startCal.getTime();
     }
 
     if (type == 4) {
       Calendar startCal = Calendar.getInstance();
       startCal.set(year, month, day, 0, 0, 0);
       startCal.setFirstDayOfWeek(2);
       startCal.set(7, 2);
       start = getStartDay(startCal.getTime());
 
       Calendar endCal = Calendar.getInstance();
       endCal.set(year, month, day, 23, 59, 59);
       endCal.setFirstDayOfWeek(2);
       endCal.set(7, 1);
       end = getEndDay(endCal.getTime());
     }
 
     if (type == 5) {
       Calendar startCal = Calendar.getInstance();
       startCal.set(year, month, day, 0, 0, 0);
       start = startCal.getTime();
 
       Calendar endCal = Calendar.getInstance();
       endCal.set(year, month, day, 23, 59, 59);
       end = endCal.getTime();
     }
     paire.setStart(start);
     paire.setEnd(end);
     return paire;
   }
 
   public static Date getStartDay(Date date)
   {
     Calendar cal = Calendar.getInstance();
     cal.setTime(date);
     cal.set(11, 0);
     cal.set(12, 0);
     cal.set(13, 0);
     return cal.getTime();
   }
 
   public static Date getEndDay(Date date)
   {
     Calendar cal = Calendar.getInstance();
     cal.setTime(date);
     cal.set(11, 23);
     cal.set(12, 59);
     cal.set(13, 59);
     return cal.getTime();
   }
 
   public static String getCurrentTime() {
     return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
   }
 
   public static String getCurrentTimeByShort() {
     return formatDate(new Date(), "yyyy-MM-dd");
   }
 
   public static String getPreCurrentTime(int hour) {
     Calendar cal = Calendar.getInstance();
     cal.setTime(new Date());
     cal.add(11, -hour);
     return formatDate(cal.getTime(), "yyyy-MM-dd HH:mm:ss");
   }
 
   public static boolean isToday(Date date)
   {
     if (date == null) {
       return false;
     }
     String dataStr = dateToString(date, "yyyy-MM-dd");
     String todayStr = dateToString(new Date(), "yyyy-MM-dd");
     return dataStr.equals(todayStr);
   }
 
   public static Date getEndHourLastSecond(Date date, int modifyNumber)
   {
     Calendar cal = Calendar.getInstance();
     cal.setTime(date);
     cal.set(12, modifyNumber);
     cal.set(13, modifyNumber);
     return cal.getTime();
   }
 
   public static double subByHourD(Date befDdate, Date afterDate)
   {
     long sub = afterDate.getTime() - befDdate.getTime();
     return sub / 3600000.0D;
   }
 
   public static int subByDay(Date befDdate, Date afterDate)
   {
     long sub = afterDate.getTime() - befDdate.getTime();
     return (int)(sub / 86400000L);
   }
 
   public static double subByDayD(Date befDdate, Date afterDate)
   {
     long sub = afterDate.getTime() - befDdate.getTime();
     return sub / 86400000.0D;
   }
 
   public static int subByMonth(Date date1, Date date2)
   {
     int result = 0;
     Calendar c1 = getCalendar(date1);
     Calendar c2 = getCalendar(date2);
     result = (c1.get(1) - c2.get(1)) * 12 + c1.get(2) - c2.get(2);
     return result == 0 ? 1 : Math.abs(result) + 1;
   }
 
   public static int subByHour(Date befDdate, Date afterDate)
   {
     long sub = afterDate.getTime() - befDdate.getTime();
     return (int)(sub / 3600000L);
   }
 
   public static double subByMinuteD(Date befDdate, Date afterDate)
   {
     long sub = afterDate.getTime() - befDdate.getTime();
     return sub / 60000.0D;
   }
 
   public static int subByMinute(Date befDdate, Date afterDate)
   {
     long sub = afterDate.getTime() - befDdate.getTime();
     return (int)(sub / 60000L);
   }
 
   public static int subBySecond(Date befDdate, Date afterDate)
   {
     long sub = afterDate.getTime() - befDdate.getTime();
     return (int)(sub / 1000L);
   }
 
   public static DatePaire getDatePaireForYear(int year)
   {
     DatePaire paire = new DatePaire();
 
     Calendar startCal = Calendar.getInstance();
     startCal.set(year, 0, 1, 0, 0, 0);
     paire.setStart(startCal.getTime());
 
     startCal.set(2, 11);
     int lastDay = startCal.getActualMaximum(5);
     startCal.set(year, 11, lastDay, 23, 59, 59);
     paire.setEnd(startCal.getTime());
 
     return paire;
   }
 
   public static DatePaire getDatePaireForMonth(int year, int month)
   {
     DatePaire paire = new DatePaire();
 
     Calendar startCal = Calendar.getInstance();
     startCal.set(year, month, 1, 0, 0, 0);
     paire.setStart(startCal.getTime());
 
     int lastDay = startCal.getActualMaximum(5);
     startCal.set(year, month, lastDay, 23, 59, 59);
     paire.setEnd(startCal.getTime());
 
     return paire;
   }
 
   public static DatePaire getDatePaireForWeeks(int year, int month, int weekIndex)
   {
     DatePaire paire = new DatePaire();
     Calendar startCal = Calendar.getInstance();
     startCal.set(1, year);
     startCal.set(2, month);
 
     startCal.setFirstDayOfWeek(2);
 
     startCal.set(4, weekIndex);
 
     if (weekIndex == 1)
       startCal.set(5, 1);
     else {
       startCal.set(7, 2);
     }
 
     paire.setStart(getStartDay(startCal.getTime()));
 
     startCal.set(7, 1);
 
     paire.setEnd(getEndDay(startCal.getTime()));
 
     return paire;
   }
 
   public static Date getStartDayOfWeek(Date date)
   {
     Calendar c1 = getCalendar(date);
     int year = c1.get(1);
     int month = c1.get(2);
     int weekIndex = getDayOfWeek(date);
     DatePaire paire = getDatePaireForWeeks(year, month, weekIndex);
     return paire.getStart();
   }
 
   public static Date getEndDayOfWeek(Date date)
   {
     Calendar c1 = getCalendar(date);
     int year = c1.get(1);
     int month = c1.get(2);
     int weekIndex = getDayOfWeek(date);
     DatePaire paire = getDatePaireForWeeks(year, month, weekIndex);
     return paire.getEnd();
   }
 
   public static DatePaire getDatePaire(Date date)
   {
     DatePaire paire = new DatePaire();
     paire.setStart(getStartDay(date));
     paire.setEnd(getEndDay(date));
     return paire;
   }
 
   public static List<DatePaire> getDatePairesForMonth(int year, int month)
   {
     List paires = new ArrayList();
 
     Calendar startCal = Calendar.getInstance();
     startCal.set(1, year);
     startCal.set(2, month);
 
     int lastDay = startCal.getActualMaximum(5);
 
     for (int i = 1; i <= lastDay; i++) {
       DatePaire paire = new DatePaire();
       startCal.set(year, month, i, 0, 0, 0);
       paire.setStart(startCal.getTime());
 
       startCal.set(year, month, i, 23, 59, 59);
       paire.setEnd(startCal.getTime());
 
       paires.add(paire);
     }
 
     return paires;
   }
 
   public static List<DatePaire> getDatePairesForWeeks(int year, int month, int weekIndex)
   {
     List paires = new ArrayList();
 
     DatePaire wpaire = getDatePaireForWeeks(year, month, weekIndex);
     Calendar cal = Calendar.getInstance();
 
     cal.setTime(wpaire.getStart());
     int startDay = cal.get(5);
 
     cal.setTime(wpaire.getEnd());
     int endDay = cal.get(5);
 
     for (int i = startDay; i <= endDay; i++) {
       DatePaire paire = new DatePaire();
       cal.set(year, month, i, 0, 0, 0);
       paire.setStart(cal.getTime());
 
       cal.set(year, month, i, 23, 59, 59);
       paire.setEnd(cal.getTime());
 
       paires.add(paire);
     }
     return paires;
   }
 
   public static int getMonthWeekCount(int year, int month)
   {
     Calendar ca = Calendar.getInstance();
     ca.set(1, year);
     ca.set(2, month);
     ca.setFirstDayOfWeek(2);
     return ca.getActualMaximum(4);
   }
 
   public static Date toDate(Date date, String pattern)
   {
     return parseDate(formatDate(date, pattern), pattern);
   }
 
   public static String getCurrentMonthStart()
   {
     DatePaire paire = getDatePaire(new Date(), 2);
     return dateToString(paire.getStart(), "yyyy-MM-dd HH:mm:ss");
   }
 
   public static String getMonthStart(Date date)
   {
     DatePaire paire = getDatePaire(date, 2);
     return dateToString(paire.getStart(), "yyyy-MM-dd HH:mm:ss");
   }
 
   public static Date getCurMonthStart()
   {
     DatePaire paire = getDatePaire(new Date(), 2);
     return paire.getStart();
   }
 
   public static Date getMonthStartDate(Date date)
   {
     DatePaire paire = getDatePaire(date, 2);
     return paire.getStart();
   }
 
   public static Date getMonthEndDate(Date date)
   {
     DatePaire paire = getDatePaire(date, 2);
     return paire.getEnd();
   }
 
   public static Date getCurMonthEnd()
   {
     DatePaire paire = getDatePaire(new Date(), 2);
     return paire.getEnd();
   }
 
   public static void main(String[] ss)
   {
     Date start = parseDate("22:10", "HH:mm");
     Date end = parseDate("06:10", "HH:mm");
     Date date = parseDate("01:10", "HH:mm");
     System.out.println(nearIn24H(start, end, date));
   }
 
   public static void testBetweenByTime() {
     Date start = parseDate("22:10", "HH:mm");
     Date end = parseDate("22:10", "HH:mm");
     Date beginWorkTime = parseDate("2014-10-10 4:01:20", "yyyy-MM-dd HH:mm:ss");
 
     System.out.println(betweenIn24H(start, end, beginWorkTime));
   }
 
   public static String getCurrentDate(String pattern)
   {
     return dateToString(new Date(), pattern);
   }
 
   public static boolean compareHour(Date first, Date second)
   {
     boolean result = false;
     Calendar firstCal = Calendar.getInstance();
     firstCal.setTime(first);
     int hour = firstCal.get(11);
     int minute = firstCal.get(12);
 
     Calendar secondCal = Calendar.getInstance();
     secondCal.setTime(new Date());
     int hour2 = secondCal.get(11);
     int minute2 = secondCal.get(12);
 
     if (hour > hour2)
       result = true;
     else if (hour < hour2) {
       result = false;
     }
     else if (minute > minute2)
       result = true;
     else {
       result = false;
     }
 
     return result;
   }
 
   public static int compareTime(Date first, Date second)
   {
     String f = dateToString(first, "HH:mm:ss");
     String s = dateToString(second, "HH:mm:ss");
     return f.compareTo(s);
   }
 
   public static String formatSecond(Integer second)
   {
     String html = "0";
     String format;
     Object[] array=null;
     if (second != null) {
       Integer s = second;
 
       Integer hours = Integer.valueOf(s.intValue() / 3600);
       Integer minutes = Integer.valueOf(s.intValue() / 60 - hours.intValue() * 60);
       Integer seconds = Integer.valueOf(s.intValue() - minutes.intValue() * 60 - hours.intValue() * 60 * 60);
       if (hours.intValue() > 0) {
          format = "%1$,d:%2$,d:%3$,d";
          array = new Object[] { hours, minutes, seconds };
       }
       else
       {
    
         if (minutes.intValue() > 0)
         {
           format = "%1$,d:%2$,d:%3$,d";
           hours = Integer.valueOf(0);
           array = new Object[] { hours, minutes, seconds };
         }
         else {
           hours = Integer.valueOf(0);
           minutes = Integer.valueOf(0);
           format = "%1$,d:%2$,d:%3$,d";
           array = new Object[] { hours, minutes, seconds };
         }
       }
       html = String.format(format, array);
     }
 
     return html;
   }
 
   public static long subDate(Date now, Date old)
   {
     Calendar nowDate = Calendar.getInstance();
     Calendar oldDate = Calendar.getInstance();
     nowDate.setTime(now);
     oldDate.setTime(old);
     long timeNow = nowDate.getTimeInMillis();
     long timeOld = oldDate.getTimeInMillis();
     long dayNum = (timeNow - timeOld) / 86400000L;
 
     return dayNum;
   }
 
   public static long getTime(Date d)
   {
     return d.getTime();
   }
 
   public static List<String> getMonthBetweenDate(Date startDate, Date endDate)
   {
     List months = new ArrayList();
 
     Calendar begin = getCalendar(startDate);
     Calendar end = getCalendar(endDate);
     while (begin.before(end)) {
       int y = begin.get(1);
       int m = begin.get(2) + 1;
       String month = m + "";
       if (m < 10) {
         month = "0" + m + "";
       }
       months.add(y + "-" + month);
       begin.add(2, 1);
     }
     return months;
   }
 
   public static int getCurYear()
   {
     int year = getYear(new Date());
     return year;
   }
 }
