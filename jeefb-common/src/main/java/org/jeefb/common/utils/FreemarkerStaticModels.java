package org.jeefb.common.utils;
import freemarker.ext.beans.BeansWrapper;
 import freemarker.template.TemplateHashModel;
 import java.util.HashMap;
 import java.util.Properties;
 import java.util.Set;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
 public class FreemarkerStaticModels extends HashMap<Object, Object>
 {
   private static final Logger logger = LoggerFactory.getLogger(FreemarkerStaticModels.class);
   private static final long serialVersionUID = -4527273072037274041L;
   private static FreemarkerStaticModels FREEMARKER_STATIC_MODELS = new FreemarkerStaticModels();
   private Properties staticModels;
 
   public static FreemarkerStaticModels getInstance()
   {
     return FREEMARKER_STATIC_MODELS;
   }
 
   public Properties getStaticModels() {
     return this.staticModels;
   }
 
   public void setStaticModels(Properties staticModels) {
     if ((this.staticModels == null) && (staticModels != null)) {
       this.staticModels = staticModels;
       Set<String> keys = this.staticModels.stringPropertyNames();
       for (String key : keys)
         FREEMARKER_STATIC_MODELS.put(key, useStaticPackage(this.staticModels.getProperty(key)));
     }
   }
 
   public static TemplateHashModel useStaticPackage(String packageName)
   {
     try {
       BeansWrapper wrapper = BeansWrapper.getDefaultInstance();
       TemplateHashModel staticModels = wrapper.getStaticModels();
       return (TemplateHashModel)staticModels.get(packageName);
     }
     catch (Exception e) {
       logger.error("*", e);
     }
     return null;
   }
 }