package org.jeefb.modules.sys.dao;

import java.util.List;

import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysFunction;
import org.jeefb.modules.sys.vo.DataRuleVO;
@MyBatisDao
public interface SysFunctionMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysFunction record);

    int insertSelective(SysFunction record);

    SysFunction selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysFunction record);

    int updateByPrimaryKey(SysFunction record);
    /**
     * 
     * @Description: 查询角色所有功能信息
     * @param userId
     * @return    
     * @return List<SysFunction>
     */
    List<SysFunction> selectRoleFunctionByUserId(String userId);
    /**
     * 
     * @Description: 查询组织所有功能信息
     * @param userId
     * @return    
     * @return List<SysFunction>
     */
    List<SysFunction> selectOrgFunctionByUserId(String userId);
    
    /**
     * 
     * @Description: 根据路径获取菜单ID
     * @param functionUrl
     * @return    
     * @return List<SysFunction>
     */
    List<SysFunction>  selectByFunctionUrl(String functionUrl);
}