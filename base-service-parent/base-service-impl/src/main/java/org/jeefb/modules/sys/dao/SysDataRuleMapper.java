package org.jeefb.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysDataRule;
import org.jeefb.modules.sys.vo.DataRuleVO;
@MyBatisDao
public interface SysDataRuleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysDataRule record);

    int insertSelective(SysDataRule record);

    SysDataRule selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysDataRule record);

    int updateByPrimaryKey(SysDataRule record);
    /**
     * 
     * @Description: 查询菜单下所有数据权限
     * @return    
     * @return List<DataRuleVO>
     */
    List<DataRuleVO>  selectByFunctionIds(@Param("functionIds")List<String> functionIds);
    
    
}