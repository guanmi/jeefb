package org.jeefb.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysUser;
import org.jeefb.modules.sys.vo.UserInfoVO;
@MyBatisDao
public interface SysUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);
    /**
     * 
     * @Description: 按用户信息进行查询
     * @param userInfoVO
     * @return    
     * @return List<UserInfoVO>
     */
    List<UserInfoVO> selectBySysUser(UserInfoVO userInfoVO);
    /**
     * 
     * @Description: 根据用户名进行查询，供登录使用
     * @param userName
     * @return    
     * @return SysUser
     */
    SysUser selectByUserName(@Param("userName")String userName);
    /**
     * 
     * @Description: 批量删除
     * @param ids
     * @return    
     * @return int
     */
    int deleteBatchByIds(List<String> ids);
}