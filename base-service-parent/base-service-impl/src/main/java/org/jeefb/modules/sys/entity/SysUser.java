package org.jeefb.modules.sys.entity;

import org.jeefb.common.persistence.BaseEntity;
/**
 * 
 * @ClassName: SysUser 
 * @Description: 后台管理员
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 下午1:23:28 
 *
 */
public class SysUser  extends BaseEntity{
	private static final long serialVersionUID = 1L;

	private String id;

    private String password;

    private String realName;

    private String status;

    private String userSalt;

    private String userName;


    private String email;

    private String phone;

  

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getUserSalt() {
        return userSalt;
    }

    public void setUserSalt(String userSalt) {
        this.userSalt = userSalt == null ? null : userSalt.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

   
}