package org.jeefb.modules.sys.dao;

import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysDictGroup;
@MyBatisDao
public interface SysDictGroupMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysDictGroup record);

    int insertSelective(SysDictGroup record);

    SysDictGroup selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysDictGroup record);

    int updateByPrimaryKey(SysDictGroup record);
}