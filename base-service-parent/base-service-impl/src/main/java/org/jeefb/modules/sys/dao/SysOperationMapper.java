package org.jeefb.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysOperation;
import org.jeefb.modules.sys.vo.OperationVO;
@MyBatisDao
public interface SysOperationMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysOperation record);

    int insertSelective(SysOperation record);

    SysOperation selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysOperation record);

    int updateByPrimaryKey(SysOperation record);
    /**
     * 
     * @Description: 查询下菜单操作权限
     * @param functionId
     * @return     
     * @return List<OperationVO>
     */
    List<OperationVO> selectByFunctionIds(@Param("functionIds")List<String> functionIds);
}