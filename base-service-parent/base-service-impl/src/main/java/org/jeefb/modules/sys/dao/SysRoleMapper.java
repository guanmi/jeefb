package org.jeefb.modules.sys.dao;

import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysRole;
@MyBatisDao
public interface SysRoleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);
}