package org.jeefb.modules.sys.service.impl;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.jeefb.common.ServiceResult;
import org.jeefb.common.utils.ExceptionUtils;
import org.jeefb.core.redis.RedisKVManager;
import org.jeefb.modules.sys.service.CachedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
@Service("cachedService")
public class CachedServiceImpl implements CachedService {
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
	@Autowired
	private RedisKVManager redisKVManager;

	/**
	 * 
	 * @Description: 从缓存内获取数据
	 * @param  key
	 * @param @return    
	 * @return ServiceResult<String>
	 */
	public ServiceResult<String> getCachedObjectByKey(String key){
		ServiceResult<String> result = new ServiceResult<String>();
		try {
			String jsondata =(String) redisKVManager.get(key);
			result.setResult(jsondata);
		} catch (Exception e) {
			logger.error(ExceptionUtils.printExceptionInfo(e, key));
			result.setSuccess(false);
		}
		return result;
	}
	
	/**
	 * 
	 * @Description: 将数据持久至缓存
	 * @param key
	 * @param  obj
	 * @param @return    
	 * @return ServiceResult<Boolean>
	 */
	public ServiceResult<Boolean> setCachedObjectByKey(String key ,Object obj){
		ServiceResult<Boolean> result = new ServiceResult<Boolean>();
		try {
			redisKVManager.set(key, JSON.toJSONString(obj));
			result.setResult(true);
		} catch (Exception e) {
			logger.error(ExceptionUtils.printExceptionInfo(e, key));
			result.setSuccess(false);
		}
		return result;
	}
	
	
	
	/**
	 * 
	 * @Description: 将数据持久至缓存有失效时间
	 * @param  key
	 * @param  obj
	 * @param  l
	 * @param  timeUnit
	 * @param @return    
	 * @return ServiceResult<Boolean>
	 */
	public ServiceResult<Boolean> setCachedObjectByKey(String key ,Object obj ,long l , TimeUnit timeUnit){
		ServiceResult<Boolean> result = new ServiceResult<Boolean>();
		try {
			redisKVManager.set(key, JSON.toJSONString(obj) ,l ,timeUnit);
			result.setResult(true);
		} catch (Exception e) {
			logger.error(ExceptionUtils.printExceptionInfo(e, key));
			result.setSuccess(false);
		}
		return result;
	}
	
	/**
	 * 
	 * @Description: 将数据持久至缓存
	 * @param key
	 * @param  obj
	 * @param @return    
	 * @return ServiceResult<Boolean>
	 */
	public ServiceResult<Boolean> setCachedByKey(String key ,String obj){
		ServiceResult<Boolean> result = new ServiceResult<Boolean>();
		try {
			redisKVManager.set(key, obj);
			result.setResult(true);
		} catch (Exception e) {
			logger.error(ExceptionUtils.printExceptionInfo(e, key));
			result.setSuccess(false);
		}
		return result;
	}
	
	
	
	/**
	 * 
	 * @Description: 将数据持久至缓存有失效时间
	 * @param  key
	 * @param  obj
	 * @param  l
	 * @param  timeUnit
	 * @param @return    
	 * @return ServiceResult<Boolean>
	 */
	public ServiceResult<Boolean> setCachedByKey(String key ,String obj ,long l , TimeUnit timeUnit){
		ServiceResult<Boolean> result = new ServiceResult<Boolean>();
		try {
			redisKVManager.set(key, obj ,l ,timeUnit);
			result.setResult(true);
		} catch (Exception e) {
			logger.error(ExceptionUtils.printExceptionInfo(e, key));
			result.setSuccess(false);
		}
		return result;
	}

}
