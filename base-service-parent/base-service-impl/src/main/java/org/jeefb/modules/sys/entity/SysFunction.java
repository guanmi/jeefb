package org.jeefb.modules.sys.entity;

public class SysFunction {
	private String id;

	private String functionLevel;

	private String functionName;

	private String functionOrder;

	private String functionUrl;

	private String parentId;

	private String functionType;

	private String functionStyle;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getFunctionLevel() {
		return functionLevel;
	}

	public void setFunctionLevel(String functionLevel) {
		this.functionLevel = functionLevel == null ? null : functionLevel.trim();
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName == null ? null : functionName.trim();
	}

	public String getFunctionOrder() {
		return functionOrder;
	}

	public void setFunctionOrder(String functionOrder) {
		this.functionOrder = functionOrder == null ? null : functionOrder.trim();
	}

	public String getFunctionUrl() {
		return functionUrl;
	}

	public void setFunctionUrl(String functionUrl) {
		this.functionUrl = functionUrl == null ? null : functionUrl.trim();
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId == null ? null : parentId.trim();
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType == null ? null : functionType.trim();
	}

	public String getFunctionStyle() {
		return functionStyle;
	}

	public void setFunctionStyle(String functionStyle) {
		this.functionStyle = functionStyle == null ? null : functionStyle.trim();
	}

	public boolean equals(Object obj) {
		SysFunction s = (SysFunction) obj;
		return id.equals(s.id);
	}

	public int hashCode() {
		String in = id;
		return in.hashCode();
	}
}