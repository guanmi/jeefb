package org.jeefb.modules.sys.dao;

import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysMutiLang;
@MyBatisDao
public interface SysMutiLangMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysMutiLang record);

    int insertSelective(SysMutiLang record);

    SysMutiLang selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysMutiLang record);

    int updateByPrimaryKey(SysMutiLang record);
}