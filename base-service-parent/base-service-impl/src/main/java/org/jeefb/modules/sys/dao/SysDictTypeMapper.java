package org.jeefb.modules.sys.dao;

import org.jeefb.common.persistence.annotation.MyBatisDao;
import org.jeefb.modules.sys.entity.SysDictType;
@MyBatisDao
public interface SysDictTypeMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysDictType record);

    int insertSelective(SysDictType record);

    SysDictType selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysDictType record);

    int updateByPrimaryKey(SysDictType record);
}