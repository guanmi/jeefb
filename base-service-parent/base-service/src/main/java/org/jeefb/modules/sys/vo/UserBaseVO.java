package org.jeefb.modules.sys.vo;

import java.io.Serializable;
/**
 * 
 * @ClassName: UserVO 
 * @Description: 用户信息
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 上午11:22:30 
 *
 */
public class UserBaseVO implements Serializable{
	private static final long serialVersionUID = 1L;

	private String id;
    private String realName;
    private String userName;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	private String email;

    private String phone;

	
    
    
}
