package org.jeefb.modules.sys.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @ClassName: UserVO
 * @Description: 用户值对象
 * @author genemax1107@aliyun.com
 * @date 2016年5月28日 上午10:01:30
 *
 */
public class UserVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<MenuTreeVO> functions;// 菜单
	private UserBaseVO userBaseVO;//

	public List<MenuTreeVO> getFunctions() {
		return functions;
	}

	public void setFunctions(List<MenuTreeVO> functions) {
		this.functions = functions;
	}

	public UserBaseVO getUserBaseVO() {
		return userBaseVO;
	}

	public void setUserBaseVO(UserBaseVO userBaseVO) {
		this.userBaseVO = userBaseVO;
	}

}
