package org.jeefb.modules.sys.vo;

import java.io.Serializable;
/**
 * 
 * @ClassName: OperationVO 
 * @Description: 权限标签，控制页面内容是否显示，
 * @author genemax1107@aliyun.com
 * @date 2016年6月12日 上午10:30:40 
 *
 */
public class OperationVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;

    private String operateCode;

    private String operateName;

    private String functionId;

    private String operationType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOperateCode() {
		return operateCode;
	}

	public void setOperateCode(String operateCode) {
		this.operateCode = operateCode;
	}

	public String getOperateName() {
		return operateName;
	}

	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
    
    
    
}
