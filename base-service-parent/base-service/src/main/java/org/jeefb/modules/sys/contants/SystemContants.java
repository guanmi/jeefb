package org.jeefb.modules.sys.contants;

import java.io.Serializable;
/**
 * 
 * @ClassName: SystemContants 
 * @Description: 静态变量 
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 下午2:09:12 
 *
 */
public class SystemContants implements Serializable{
	private static final long serialVersionUID = 1L;
	public static final String LOCK_STATUS="2";//用户被锁
	public static final String NORMAL_STATUS="1";//正常状态

}
