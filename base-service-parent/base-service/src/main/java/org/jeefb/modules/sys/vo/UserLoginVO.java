package org.jeefb.modules.sys.vo;

import java.io.Serializable;
/**
 * 
 * @ClassName: UserLoginVO 
 * @Description: 用户登录实体 
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 上午11:07:31 
 *
 */
public class UserLoginVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String userName;
	private String password;
	private String verfiyCode;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerfiyCode() {
		return verfiyCode;
	}
	public void setVerfiyCode(String verfiyCode) {
		this.verfiyCode = verfiyCode;
	}
	
	

}
