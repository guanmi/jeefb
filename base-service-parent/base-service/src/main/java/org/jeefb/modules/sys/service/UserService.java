package org.jeefb.modules.sys.service;

import java.util.List;

import org.jeefb.common.PageVO;
import org.jeefb.common.ServiceResult;
import org.jeefb.modules.sys.vo.UserBaseVO;
import org.jeefb.modules.sys.vo.UserInfoVO;
import org.jeefb.modules.sys.vo.UserLoginVO;
/**
 * 
 * @ClassName: UserService 
 * @Description: 用户管理服务
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 上午11:11:16 
 *
 */
public interface UserService {
	
	/**
	  * 
	  * @Description: 根据主键删除
	  * @param id
	  * @return    
	  * @return ServiceResult<Boolean>
	  */
	 ServiceResult<Boolean> delete(String  id);
	 
	 /**
	  * 
	  * @Description: 批量删除
	  * @param ids
	  * @return    
	  * @return ServiceResult<Boolean>
	  */
	 ServiceResult<Boolean> delete(List<String>  ids);
	 /**
	  * 
	  * @Description: 保存或更新
	  * @param userInfoVO
	  * @return    
	  * @return ServiceResult<Boolean>
	  */
	 ServiceResult<Boolean> saveOrUpdate(UserInfoVO userInfoVO );
	 
	/**
	 * 
	 * @Description: 分页查询
	 * @param userInfoVO
	 * @param pageNo 
	 * @param pageSize
	 * @return    
	 * @return ServiceResult<PageInfo<UserInfoVO>>
	 */
	 ServiceResult<PageVO<UserInfoVO>> selectPage(UserInfoVO userInfoVO ,Integer pageNo ,Integer pageSize);
	/**
	 * 
	 * @Description: 根据用户信息登录系统  
	 * @param  sessionId
	 * @param userLoginVO
	 * @param @return    
	 * @return ServiceResult<UserVO>
	 */
    ServiceResult<UserBaseVO>  login(UserLoginVO userLoginVO ,String sessionId);
}
