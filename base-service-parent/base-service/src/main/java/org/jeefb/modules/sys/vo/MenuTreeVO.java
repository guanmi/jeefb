package org.jeefb.modules.sys.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @ClassName: FunctionVO
 * @Description: 菜单功能
 * @author genemax1107@aliyun.com
 * @date 2016年5月28日 上午10:03:12
 *
 */
public class MenuTreeVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;

	private String functionLevel;

	private String functionName;

	private String functionOrder;

	private String functionUrl;

	private String parentId;

	private String functionType;

	private String functionStyle;
	
	 private List<MenuTreeVO> childrens;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFunctionLevel() {
		return functionLevel;
	}

	public void setFunctionLevel(String functionLevel) {
		this.functionLevel = functionLevel;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionOrder() {
		return functionOrder;
	}

	public void setFunctionOrder(String functionOrder) {
		this.functionOrder = functionOrder;
	}

	public String getFunctionUrl() {
		return functionUrl;
	}

	public void setFunctionUrl(String functionUrl) {
		this.functionUrl = functionUrl;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public String getFunctionStyle() {
		return functionStyle;
	}

	public void setFunctionStyle(String functionStyle) {
		this.functionStyle = functionStyle;
	}

	public List<MenuTreeVO> getChildrens() {
		return childrens;
	}

	public void setChildrens(List<MenuTreeVO> childrens) {
		this.childrens = childrens;
	}
	
	
}
