package org.jeefb.modules.sys.contants;

import java.io.Serializable;

/**
 * 
 * @ClassName: SystemContants
 * @Description: 缓存静态变量
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 下午2:09:12
 *
 */
public class SystemCachedContants implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String ADMIN_USER_SESSION = "admin:user:";// 用户缓存信息
	public static final String ADMIN_VERFIY_SESSION = "admin:verfiycode:";// 用户验证码缓存信息
	public static final String ADMIN_FUNCTION_SESSION = "admin:function:";// 用户功能缓存信息
	public static final String ADMIN_OPERATION_SESSION = "admin:operation:";// 操作权限缓存信息
	public static final String ADMIN_DATA_RULE_SESSION = "admin:rule:";// 数据权限缓存信息
	public static final int ADMIN_TIME_OUT = 30;// 用户功能缓存信息

}
