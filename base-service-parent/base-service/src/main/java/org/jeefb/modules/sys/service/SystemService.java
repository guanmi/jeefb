package org.jeefb.modules.sys.service;

import java.util.List;

import org.jeefb.common.ServiceResult;
import org.jeefb.modules.sys.vo.DataRuleVO;
import org.jeefb.modules.sys.vo.UserVO;
/**
 * 
 * @ClassName: SystemService 
 * @Description: 系统管理
 * @author genemax1107@aliyun.com
 * @date 2016年5月28日 上午9:59:42 
 *
 */
public interface SystemService {
	/**
	 * 
	 * @Description: 查询用户权限信息
	 * @param userId
	 * @param sessionId
	 * @return    
	 * @return ServiceResult<UserVO>
	 */
   ServiceResult<UserVO>  getUserVO(String  userId ,String sessionId);
   
   /**
    * 
    * @Description: 获取菜单数据规则信息
    * @param requestPath
    * @return    
    * @return ServiceResult<List<DataRuleVO>>
    */
   ServiceResult<List<DataRuleVO>>  getFunctionDataRules(String  requestPath , String sessionId);
   
   /**
    * 
    * @Description: 查询路径是否在菜单功能中
    * @param requestPath
    * @return    
    * @return ServiceResult<Boolean>
    */
   ServiceResult<Boolean>  getFunctionByUrl(String  requestPath);
}
