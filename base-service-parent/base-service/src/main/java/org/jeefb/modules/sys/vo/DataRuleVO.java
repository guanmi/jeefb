package org.jeefb.modules.sys.vo;

import java.io.Serializable;

/**
 * 
 * @ClassName: DataRuleVO
 * @Description: 数据权限，针对列表菜单增加WHERE条件查询使用
 * @author genemax1107@aliyun.com
 * @date 2016年6月12日 上午10:23:12
 *
 */
public class DataRuleVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String ruleName;

	private String ruleColumn;

	private String ruleConditions;

	private String ruleValue;
	private String functionId;
	private String id;

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleColumn() {
		return ruleColumn;
	}

	public void setRuleColumn(String ruleColumn) {
		this.ruleColumn = ruleColumn;
	}

	public String getRuleConditions() {
		return ruleConditions;
	}

	public void setRuleConditions(String ruleConditions) {
		this.ruleConditions = ruleConditions;
	}

	public String getRuleValue() {
		return ruleValue;
	}

	public void setRuleValue(String ruleValue) {
		this.ruleValue = ruleValue;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
