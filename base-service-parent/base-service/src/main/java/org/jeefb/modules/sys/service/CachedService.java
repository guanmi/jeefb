package org.jeefb.modules.sys.service;

import java.util.concurrent.TimeUnit;

import org.jeefb.common.ServiceResult;

/**
 * 
 * @ClassName: CachedService 
 * @Description: 缓存服务
 * @author genemax1107@aliyun.com
 * @date 2016年5月27日 下午3:58:15 
 *
 */
public interface CachedService {
	/**
	 * 
	 * @Description: 从缓存内获取数据
	 * @param  key
	 * @param @return    
	 * @return ServiceResult<String>
	 */
	ServiceResult<String> getCachedObjectByKey(String key);
	
	/**
	 * 
	 * @Description: 将数据持久至缓存
	 * @param key
	 * @param  obj
	 * @param @return    
	 * @return ServiceResult<String>
	 */
	ServiceResult<Boolean> setCachedObjectByKey(String key ,Object obj);
	
	
	
	/**
	 * 
	 * @Description: 将数据持久至缓存
	 * @param  key
	 * @param  obj
	 * @param  l
	 * @param  timeUnit
	 * @param @return    
	 * @return ServiceResult<String>
	 */
	ServiceResult<Boolean> setCachedObjectByKey(String key ,Object obj ,long l , TimeUnit timeUnit);
	
	
	/**
	 * 
	 * @Description: 将数据持久至缓存
	 * @param key
	 * @param  obj
	 * @param @return    
	 * @return ServiceResult<String>
	 */
	ServiceResult<Boolean> setCachedByKey(String key ,String obj);
	
	
	
	/**
	 * 
	 * @Description: 将数据持久至缓存
	 * @param  key
	 * @param  obj
	 * @param  l
	 * @param  timeUnit
	 * @param @return    
	 * @return ServiceResult<String>
	 */
	ServiceResult<Boolean> setCachedByKey(String key ,String obj ,long l , TimeUnit timeUnit);

}
