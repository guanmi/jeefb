package org.jeefb.modules.sys.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @ClassName: UserInfoVO
 * @Description: 用户基本信息
 * @author genemax1107@aliyun.com
 * @date 2016年5月28日 上午10:01:30
 *
 */
public class UserInfoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;

    private String password;

    private String realName;

    private String status;

    private String userSalt;

    private String userName;


    private String email;

    private String phone;
    private Date updateDate;// 更新日期

	private String updateBy;// 更新人ID
	private Date createDate;// 创建日期

	private String createBy;// 创建人ID

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserSalt() {
		return userSalt;
	}

	public void setUserSalt(String userSalt) {
		this.userSalt = userSalt;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	
	

}
