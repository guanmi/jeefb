package org.jeefb.core.redis;

import java.util.List;

import org.jeefb.core.redis.support.IHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
/**
 * 
 * @ClassName: RedisHashManager 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author guanmi@jingtum.com 
 * @date 2016年3月2日 下午4:32:53 
 *
 */
@Component("redisHashManager")
public class RedisHashManager implements IHash{
	@Autowired
    private RedisTemplate<String, Object> redisTemplate;
	
	@Override
	public void put(String objectKey,String key ,Object o){
		redisTemplate.opsForHash().put(objectKey, key, o);
	}
	@Override
	public Object get(String objectKey,String key ){
		return redisTemplate.opsForHash().get(objectKey, key);
	}
	@Override
	public void delete(String objectKey,String key ){
		redisTemplate.opsForHash().delete(objectKey, key);
	}
	@Override
	public List<Object> findByObjectKey(String objectKey){
		return redisTemplate.opsForHash().values(objectKey);
	}
	@Override
	public boolean isExist(String objectKey,String key){
		return redisTemplate.opsForHash().hasKey(objectKey, key);
	}
}
