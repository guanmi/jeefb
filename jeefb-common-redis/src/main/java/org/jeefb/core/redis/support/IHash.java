package org.jeefb.core.redis.support;

import java.util.List;
/**
 * 
 * @ClassName: IHash 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author guanmi@jingtum.com 
 * @date 2016年3月2日 下午4:33:59 
 *
 */
public interface IHash {
	public void put(String objectKey,String key ,Object o);
	
	public Object get(String objectKey,String key );
	
	public void delete(String objectKey,String key );
	
	public List<Object> findByObjectKey(String objectKey);
	
	public boolean isExist(String objectKey,String key);
}
