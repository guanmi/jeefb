package org.jeefb.core.redis.support;

import java.util.List;

import org.springframework.data.redis.core.BulkMapper;
import org.springframework.data.redis.core.query.SortQuery;
import org.springframework.data.redis.serializer.RedisSerializer;
/**
 * 
 * @ClassName: ISort 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author guanmi@jingtum.com 
 * @date 2016年3月2日 下午4:34:21 
 *
 */
public interface ISort {
	 public List<Object>  sort(SortQuery<String> query);
	 public <T> List<T> sort(SortQuery<String> query, RedisSerializer<T> resultSerializer);
	 public <T> List<T> sort(SortQuery<String> query, BulkMapper<T, Object> bulkMapper);
}
