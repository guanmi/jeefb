package org.jeefb.core.redis;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jeefb.core.redis.support.IRedisList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.google.gson.internal.Primitives;

/**
 * 
 * @ClassName: RedisListManager 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author guanmi@jingtum.com 
 * @date 2016年3月2日 下午4:31:46 
 *
 */
@Component("redisListManager")
public class RedisListManager implements IRedisList<String> {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

   
    public List<Object> range(String key, long start, long end) {
        return redisTemplate.opsForList().range(key, start, end);
    }

   
    public void trim(String key, long start, long end) {
        redisTemplate.opsForList().trim(key, start, end);
    }

   
    public Long size(String key) {
        return redisTemplate.opsForList().size(key);
    }

   
    public Long leftPush(String key, Object value) {
        return redisTemplate.opsForList().leftPush(key, value);
    }

   
    public Long leftPushAll(String key, Object... values) {
        return redisTemplate.opsForList().leftPushAll(key, values);
    }

   
    public Long leftPushIfPresent(String key, Object value) {
        return redisTemplate.opsForList().leftPushIfPresent(key, value);
    }

   
    public Long leftPush(String key, Object pivot, Object value) {
        return redisTemplate.opsForList().leftPush(key, pivot, value);
    }

   
    public Long rightPush(String key, Object value) {
        return redisTemplate.opsForList().rightPush(key, value);
    }

   
    public Long rightPushAll(String key, Object... values) {
        return redisTemplate.opsForList().rightPushAll(key, values);
    }

   
    public Long rightPushIfPresent(String key, Object value) {
        return redisTemplate.opsForList().rightPushIfPresent(key, value);
    }

   
    public Long rightPush(String key, Object pivot, Object value) {
        return redisTemplate.opsForList().rightPush(key, pivot, value);
    }

   
    public void set(String key, long index, Object value) {
        redisTemplate.opsForList().set(key, index, value);
    }

   
    public Long remove(String key, long i, Object value) {
        return redisTemplate.opsForList().remove(key, i, value);
    }

   
    public <T> T index(String key, long index, Class<T> classOfT) {
        return Primitives.wrap(classOfT).cast(redisTemplate.opsForList().index(key, index));
    }

   
    public void delete(String key) {
        redisTemplate.delete(key);
    }

   
    public void deleteAll(Collection<String> keys) {
        redisTemplate.delete(keys);
    }

   
    public <T> T leftPop(String key, Class<T> classOfT) {
        return  Primitives.wrap(classOfT).cast(redisTemplate.opsForList().leftPop(key));
    }

   
    public <T> T leftPop(String key, Class<T> classOfT, long timeout, TimeUnit unit) {
        return Primitives.wrap(classOfT).cast(redisTemplate.opsForList().leftPop(key, timeout, unit));
    }

   
    public <T> T rightPop(String key, Class<T> classOfT) {
        return Primitives.wrap(classOfT).cast(redisTemplate.opsForList().rightPop(key));
    }

   
    public <T> T rightPop(String key, Class<T> classOfT, long timeout, TimeUnit unit) {
        return Primitives.wrap(classOfT).cast(redisTemplate.opsForList().rightPop(key, timeout, unit));
    }

   
    public <T> T rightPopAndLeftPush(String sourceKey, String destinationKey, Class<T> classOfT) {
        return Primitives.wrap(classOfT).cast(redisTemplate.opsForList().rightPopAndLeftPush(sourceKey, destinationKey));
    }

   
    public <T> T rightPopAndLeftPush(String sourceKey, String destinationKey, Class<T> classOfT,
                                     long timeout, TimeUnit unit) {
        return Primitives.wrap(classOfT).cast(redisTemplate.opsForList().rightPopAndLeftPush(sourceKey, destinationKey, timeout, unit));
    }
    
    public  List<Object> range(String sourceKey,int start,int end){
      return	redisTemplate.opsForList().range(sourceKey, start, end);
    }

    
    /**
     * ============域访问器==============
     */
    public RedisTemplate<String, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
}
