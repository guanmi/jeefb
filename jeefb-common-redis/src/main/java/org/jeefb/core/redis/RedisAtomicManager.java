package org.jeefb.core.redis;

import java.util.concurrent.TimeUnit;

import org.jeefb.core.redis.support.IAtomic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

/**
 * 
 * @ClassName: RedisAtomicManager 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author guanmi@jingtum.com 
 * @date 2016年3月2日 下午4:32:32 
 *
 */
@Component("redisAtomicManager")
public class RedisAtomicManager implements IAtomic<String> {

    @Autowired
    private RedisTemplate<String, Long> redisTemplate;

   
    public void set(String key, long value) {
        RedisAtomicLong entityIdCounter = new RedisAtomicLong(key, redisTemplate.getConnectionFactory());
        entityIdCounter.set(value);
    }

   
    public void set(String key, long value, long timeout, TimeUnit unit) {
        RedisTemplate<String, Long> redisTemplate = new RedisTemplate<String, Long>();
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new GenericToStringSerializer<Long>(Long.class));
        redisTemplate.setExposeConnection(true);
        redisTemplate.setConnectionFactory(this.redisTemplate.getConnectionFactory());
        redisTemplate.afterPropertiesSet();
        redisTemplate.opsForValue().set(key, value, timeout, unit);
    }

   
    public Long incrementAndGet(String key, long delta) {
        return redisTemplate.opsForValue().increment(key, delta);
    }

   
    public Long decrementAndGet(String key, long delta) {
        return redisTemplate.opsForValue().increment(key, -delta);
    }

   
    public Long getLong(String key) {
        if (isExist(key))
            return redisTemplate.opsForValue().increment(key, 0);
        else
            return null;
    }

   
    public void delete(String key) {
        redisTemplate.delete(key);
    }

   
    public boolean isExist(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * ============域访问器==============
     */

    public RedisTemplate<String, Long> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Long> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
}
