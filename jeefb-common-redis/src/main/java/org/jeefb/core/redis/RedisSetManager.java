package org.jeefb.core.redis;

import java.util.Set;

import org.jeefb.core.redis.support.IRedisSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.google.gson.internal.Primitives;

/**
 * 
 * @ClassName: RedisSetManager 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author guanmi@jingtum.com 
 * @date 2016年3月2日 下午4:32:08 
 *
 */
@Component("redisSetManager")
public class RedisSetManager implements IRedisSet<String> {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

   
    public void add(String key, Object value) {
        redisTemplate.opsForSet().add(key, value);
    }

   
    public boolean isMemberOf(String key, Object value) {
        return redisTemplate.opsForSet().isMember(key, value);
    }

   
    public Set<Object> members(String key) {
        return redisTemplate.opsForSet().members(key);
    }

   
    public <T> T pop(String key, Class<T> classOfT) {
        return Primitives.wrap(classOfT).cast(redisTemplate.opsForSet().pop(key));
    }

   
    public void delete(String key) {
        redisTemplate.delete(key);
    }

    /**
     * ============域访问器==============
     */
    
    public RedisTemplate<String, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
    
    
}
