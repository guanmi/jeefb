package org.jeefb.core.redis.support;

import java.util.Set;
/**
 * 
 * @ClassName: IRedisSet 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author guanmi@jingtum.com 
 * @date 2016年3月2日 下午4:34:13 
 * 
 * @param <K>
 */
public interface IRedisSet<K> {

    void add(K key, Object value);

    boolean isMemberOf(K key, Object value);

    Set<Object> members(K key);

    <T> T pop(K key, Class<T> classOfT);

    void delete(K key);

}
