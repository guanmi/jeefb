package org.jeefb.core.redis;

import java.util.List;

import org.jeefb.core.redis.support.ISort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BulkMapper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.query.SortQuery;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
@Component("redisSortManager")
public class RedisSortManager implements ISort {
	 @Autowired
	 private RedisTemplate<String, Object> redisTemplate;
	 
	 public List<Object>  sort(SortQuery<String> query){
		 return redisTemplate.sort(query);
	 }
	 public <T> List<T> sort(SortQuery<String> query, RedisSerializer<T> resultSerializer){
		 return redisTemplate.sort(query, resultSerializer);
	 }
	 
	 public <T> List<T> sort(SortQuery<String> query, BulkMapper<T, Object> bulkMapper) {
		 return redisTemplate.sort(query, bulkMapper);
	 }
	 
	 
	 
}
