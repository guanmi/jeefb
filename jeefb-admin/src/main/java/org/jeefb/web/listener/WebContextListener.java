package org.jeefb.web.listener;

import javax.servlet.ServletContext;

import org.springframework.web.context.WebApplicationContext;


public class WebContextListener extends org.springframework.web.context.ContextLoaderListener {
	
	public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {
		if (!printKeyLoadMessage()){
			return null;
		}
		return super.initWebApplicationContext(servletContext);
	}
	
	
	public static boolean printKeyLoadMessage(){
		StringBuilder sb = new StringBuilder();
		sb.append("\r\n======================================================================\r\n");
		sb.append("\r\n    欢迎使用 --JEE FAST BOOT \r\n");
		sb.append("\r\n======================================================================\r\n");
		System.out.println(sb.toString());
		return true;
	}
}
