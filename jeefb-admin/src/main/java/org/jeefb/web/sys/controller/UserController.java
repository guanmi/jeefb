package org.jeefb.web.sys.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.jeefb.common.PageVO;
import org.jeefb.common.ServiceResult;
import org.jeefb.modules.sys.service.UserService;
import org.jeefb.modules.sys.vo.UserInfoVO;
import org.jeefb.web.utils.AjaxJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("user")
public class UserController extends BaseController {
	private static final Logger logger = Logger.getLogger(UserController.class);
	@Autowired
	private UserService userService;
	

	/**
	 * 
	 * @Description: 查询用户基本信息
	 * @param request
	 * @return
	 * @return String
	 */
	@RequestMapping("index")
	public String list(HttpServletRequest request) {
		return theme + "/user/user-list";

	}
	/**
	 * 
	 * @Description: 查询分页数据
	 * @param request
	 * @param userInfoVO
	 * @param pageNo
	 * @param pageSize
	 * @return    
	 * @return PageInfo<UserInfoVO>
	 */
    @ResponseBody
    @RequestMapping("list")
	public PageVO<UserInfoVO> getList(HttpServletRequest request , UserInfoVO userInfoVO,
			@RequestParam(value = "offset", defaultValue = "0") Integer offset,
			@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
    	logger.debug("UserController list method params:"+ JSON.toJSONString(userInfoVO) + " offset:"+offset+" limit:"+limit);
    	//从缓存中获取数据权限
    	this.setDataRule(userInfoVO);
		ServiceResult<PageVO<UserInfoVO>> result = userService.selectPage(userInfoVO, offset, limit);
		if (result.getSuccess()) {
			PageVO<UserInfoVO> page = result.getResult();
			return page;
		}
		return null;
	}
    
    /**
     * 
     * @Description: 更新或修改用户信息
     * @param userInfoVO
     * @return    
     * @return AjaxJson
     */
    @ResponseBody
    @RequestMapping("savaOrUpdate")
    public AjaxJson savaOrUpdate(UserInfoVO userInfoVO){
    	logger.debug("UserController savaOrUpdate method params:"+ JSON.toJSONString(userInfoVO));
    	AjaxJson ajaxJson = new AjaxJson();
    	ServiceResult<Boolean> result = userService.saveOrUpdate(userInfoVO);
    	if(result.getSuccess() && result.getResult()){
    		ajaxJson.setMsg(getLanguageContext("system.success"));
    	} else {
    		ajaxJson.setSuccess(false);	
    		ajaxJson.setMsg(getLanguageContext("system.fail"));
    	}
    	return ajaxJson;
    	
    }
    
    /**
     * 
     * @Description: 批量删除用户数据
     * @param ids
     * @return    
     * @return AjaxJson
     */
    @ResponseBody
    @RequestMapping(value = "deteleBatch" , method=RequestMethod.POST)
    public AjaxJson deteleBatch(@RequestParam(value = "ids[]",  required=true) String[] ids){
    	logger.debug("UserController deteleBatch method params:"+ ids);
    	AjaxJson ajaxJson = new AjaxJson();
    	List<String> list = Arrays.asList(ids);
    	ServiceResult<Boolean> result = userService.delete(list);
    	if(result.getSuccess() && result.getResult()){
    		ajaxJson.setMsg(getLanguageContext("system.success"));
    	} else {
    		ajaxJson.setSuccess(false);	
    		ajaxJson.setMsg(getLanguageContext("system.fail"));
    	}
    	return ajaxJson;
    }
    
    /**
     * 
     * @Description: 删除用户数据
     * @param id
     * @return    
     * @return AjaxJson
     */
    @ResponseBody
    @RequestMapping(value = "detele" , method=RequestMethod.POST)
    public AjaxJson deteleBatch(@RequestParam(value = "id",  required=true) String id){
    	logger.debug("UserController detele method params:"+ id);
    	AjaxJson ajaxJson = new AjaxJson();
    	ServiceResult<Boolean> result = userService.delete(id);
    	if(result.getSuccess() && result.getResult()){
    		ajaxJson.setMsg(getLanguageContext("system.success"));
    	} else {
    		ajaxJson.setSuccess(false);	
    		ajaxJson.setMsg(getLanguageContext("system.fail"));
    	}
    	return ajaxJson;
    }
    

}
