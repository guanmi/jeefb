package org.jeefb.web.sys.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.jeefb.modules.sys.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("system")
public class SystemController extends BaseController {
	private static final Logger logger = Logger.getLogger(SystemController.class);
	@Autowired
	private SystemService systemService;
	
	@RequestMapping("main")
	public String main(HttpServletRequest request) {
		return theme + "/main/main";

	}

}
