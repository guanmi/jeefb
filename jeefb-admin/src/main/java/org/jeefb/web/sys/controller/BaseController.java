package org.jeefb.web.sys.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.jeefb.common.ServiceResult;
import org.jeefb.common.utils.ReflectUtils;
import org.jeefb.modules.sys.service.SystemService;
import org.jeefb.modules.sys.vo.DataRuleVO;
import org.jeefb.web.utils.JeeFBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;

/**
 * 
 * @ClassName: BaseController 
 * @Description: 基础控制类
 * @author genemax1107@aliyun.com
 * @date 2016年6月12日 下午5:20:06 
 *
 */
public class BaseController {
	@Value("${system.theme}")
	String theme;
	@Autowired
	protected HttpServletRequest request;
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private MessageSource messageSource;
	/**
	 * 
	 * @Description: 数据权限
	 * @return    
	 * @return List<DataRuleVO>
	 */
	public  List<DataRuleVO> getDataRuleVO(){
		String requestPath = JeeFBUtils.getRequestPath(request);
		String sessionId = request.getSession().getId();
		ServiceResult<List<DataRuleVO>> result =	systemService.getFunctionDataRules(requestPath, sessionId);
		if(result.getSuccess() && result.getResult() != null){
			return result.getResult();
		}
		return null;
	}
	/**
	 * 
	 * @Description:查询条件设置值
	 * @param entity
	 * @return    
	 * @return T
	 */
	public <T> T setDataRule(T entity){
		List<DataRuleVO> list = getDataRuleVO();
		if(list != null && list.size() > 0 ){
			for (DataRuleVO dataRuleVO : list) {
				ReflectUtils.setFieldValueByPropertyName(entity, dataRuleVO.getRuleName(), dataRuleVO.getRuleValue());
			}
		}
		return entity;
	}
	
	/**
	 * 
	 * @Description: 获取浏览器语言
	 * @return    
	 * @return String
	 */
	public String getLanguageContext(String code){
		String lang = request.getLocale().getLanguage();
		System.out.println("lang:"+lang);
		Locale locale =	new Locale(lang,"");
		return 	messageSource.getMessage(code, null, locale);
	}
	
	

}
