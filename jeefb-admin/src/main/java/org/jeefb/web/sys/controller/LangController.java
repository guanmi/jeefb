package org.jeefb.web.sys.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

@Controller
public class LangController {
	private static final Logger logger = Logger.getLogger(LangController.class);



	@Autowired 
	CookieLocaleResolver resolver;
    
     
    /**
     * 语言切换
     */
    @RequestMapping("language")
    public ModelAndView language(HttpServletRequest request,HttpServletResponse response,String language){
    	logger.info("语言切换");
        language=language.toLowerCase();
        if(language==null||language.equals("")){
            return new ModelAndView("redirect:/");
        }else{
            if(language.equals("zh_cn")){
                resolver.setLocale(request, response, Locale.CHINA );
            }else if(language.equals("en")){
                resolver.setLocale(request, response, Locale.ENGLISH );
            }else{
                resolver.setLocale(request, response, Locale.CHINA );
            }
        }
         
        return new ModelAndView("redirect:/");
    }

	
}
