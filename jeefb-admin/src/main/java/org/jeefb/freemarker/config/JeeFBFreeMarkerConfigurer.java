package org.jeefb.freemarker.config;


import java.io.IOException;

import org.jeefb.freemarker.tags.JeefbTags;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;


import freemarker.template.TemplateException;

public class JeeFBFreeMarkerConfigurer extends FreeMarkerConfigurer {
 
    public void afterPropertiesSet() throws IOException, TemplateException {
        super.afterPropertiesSet();
        this.getConfiguration().setSharedVariable("jee", new JeefbTags());
    }

}
