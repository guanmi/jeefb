package org.jeefb.freemarker.tags;

import org.jeefb.freemarker.directive.BlockDirective;
import org.jeefb.freemarker.directive.ExtendsDirective;
import org.jeefb.freemarker.directive.OverrideDirective;
import org.jeefb.freemarker.directive.SuperDirective;

import freemarker.template.SimpleHash;
import freemarker.template.utility.XmlEscape;

public class JeefbTags extends SimpleHash {
	private static final long serialVersionUID = 1L;

	public JeefbTags() {
        put("hasPermission", new HasPermissionTag());
        put("fmXmlEscape",new XmlEscape());
        put("block",new BlockDirective());
        put("extends",new ExtendsDirective());
        put("override",new OverrideDirective());
        put("super",new SuperDirective());
   
    }
}