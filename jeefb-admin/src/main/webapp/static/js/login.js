$(document).ready(function() {
	
	$("#userName").attr("nullmsg",pleaseinputusername);
	$("#userName").attr("title",username);
	
	$("#password").attr("nullmsg",pleaseinutpassword);
	$("#password").attr("title",password);
	
	$("#verfiyCode").attr("nullmsg",pleaseinputvalidatecode);
	$("#verfiyCode").attr("title",validatecode);
	getCookie();
});

$('#randCodeImage').click(function(){
    reloadRandCodeImage();
});
/**
 * 刷新验证码
 */
function reloadRandCodeImage() {
    var date = new Date();
    var img = document.getElementById("randCodeImage");
    img.src="${core.base}/servlet/validateCodeServlet?a=" + date.getTime();
}
// 重置
$('#resetFrom').click(function(e) {
	$(":input").each(function() {
	$('#'+this.name).val("");
	});
});
// 点击登录
$('#but_login').click(function(e) {
	submit();
});
//回车登录
$(document).keydown(function(e){
	if(e.keyCode == 13) {
		submit();
	}
});
//表单提交
function submit()
{
	var submit = true;
//	$("input[nullmsg]").each(function() {
//		if ($("#" + this.name).val() == "") {
//			showError($("#" + this.name).attr("nullmsg"), 500);
//			//jrumble();
//			setTimeout('hideTop()', 1000);
//			submit = false;
//			return false;
//		}
//	});
	if (submit) {
//		hideTop();
//		loading(checking, 1);
//		setTimeout("unloading()", 1000);
//		setTimeout("Login()", 1000);
		Login();
	}

}

//登录处理函数
function Login() {
	setCookie();
	var actionurl=$('form').attr('action');//提交路径
	var checkurl=$('form').attr('check');//验证路径
	 var formData = new Object();
	var data=$(":input").each(function() {
		 formData[this.name] =$("#"+this.name ).val();
	});
	$.ajax({
		async : false,
		cache : false,
		type : 'POST',
		url : checkurl,// 请求的action路径
		data : formData,
		error : function() {// 请求失败处理函数
		},
		success : function(data) {
			var d = $.parseJSON(data);
			alert(d);
			if (d.success) {
				alert(d.obj);
				loginsuccess(d.obj);
			} else {
			  showError(d.msg);
			}
		}
	});
}
//显示错误提示
function showError(str) {
	$('#messages').addClass('error').html(str).stop(true, true).show().animate({
		opacity : 1,
		right : '0'
	}, 500);

}
//验证通过加载动画
function loginsuccess(userId)
{
	$("#formLogin").submit();
}
//加载信息
function loading(name, overlay) {
	$('body').append('<div id="overlay"></div><div id="preloader">' + name + '..</div>');
	if (overlay == 1) {
		$('#overlay').css('opacity', 0.1).fadeIn(function() {
			$('#preloader').fadeIn();
		});
		return false;
	}
	$('#preloader').fadeIn();
}
function unloading() {
	$('#preloader').fadeOut('fast', function() {
		$('#overlay').fadeOut();
	});
}

//设置cookie
function setCookie()
{
	if ($('#on_off').val() == '1') {
		$("input[iscookie='true']").each(function() {
			$.cookie(this.name, $("#"+this.name).val(), "/",24);
			$.cookie("COOKIE_NAME","true", "/",24);
		});
	} else {
		$("input[iscookie='true']").each(function() {
			$.cookie(this.name,null);
			$.cookie("COOKIE_NAME",null);
		});
	}
}
//读取cookie
function getCookie()
{
	var COOKIE_NAME=$.cookie("COOKIE_NAME");
	if (COOKIE_NAME !=null) {
		$("input[iscookie='true']").each(function() {
			$($("#"+this.name).val( $.cookie(this.name)));
            if("admin" == $.cookie(this.name)) {
                $("#randCode").focus();
            } else {
                $("#password").val("");
                $("#password").focus();
            }
        });
		$("#on_off").attr("checked", true);
		$("#on_off").val("1");
	} 
	else
	{
		$("#on_off").attr("checked", false);
		$("#on_off").val("0");
        $("#randCode").focus();
	}
}

function hideTop() {
	$('#messages').animate({
		opacity : 0,
		right : '-20'
	}, 500, function() {
		$(this).hide();
	});
}