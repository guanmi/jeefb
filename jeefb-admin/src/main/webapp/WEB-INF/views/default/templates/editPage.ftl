<#assign pagecss="jeeui-page-edit" />
<#if !scriptModuleName??><#assign scriptModuleName="base-page-edit"></#if>
<#if !meltipartForm??><#assign meltipartForm=false></#if>
<#if meltipartForm><#assign meltipartFormAttribute="enctype=\"multipart/form-data\""></#if>

<@jee.override name="body">
<div class="jeeui-page jeeui-page-edit">
    <div class="jeeui-bg-danger"></div>
    <form action="" method="post" class="jeeui-form-fixed" ${meltipartFormAttribute} role="form">
        <@jee.block name="edit-body"></@jee.block>
    </form>
    <@jee.block name="edit-body-extends"></@jee.block>
</div>
</@jee.override>

<@jee.extends name="/templates/default/page.ftl"/>