<#assign pagecss="jeeui-page-selector" />
<#assign pageHeight="560" />
<#assign pageWidth="1300" />
<#if !scriptModuleName??><#assign scriptModuleName="base-page-selector"></#if>
<#if !useBreadcrumbs??><#assign useBreadcrumbs=false /></#if>
<#if !useToolbar??><#assign useToolbar=false /></#if>

<@jee.override name="page-head">
<div class="jeeui-page-condition">
    <form class="jeeui-form-condition jeeui-form-flow">
        <input type="hidden" class="jeeui-order-field" name="orderField" value=""/>
        <input type="hidden" class="jeeui-order-direction" name="orderDirection" value="asc"/>
        <input type="hidden" class="jeeui-paging-page" name="start" value="1"/>
        <input type="hidden" class="jeeui-paging-size" name="limit" value="10"/>
        
        <@jee.block name="condition"></@jee.block>

        <div class="jeeui-form-group jeeui-form-button jeeui-form-checkAll">
            <div class="jeeui-form-controls">
                <div class="jeeui-checkbox-group">
                    <input type="checkbox" name="checkAll" value="option2"><label>全选</label>
                </div>
            </div>
        </div>
        <div class="jeeui-form-group jeeui-form-button">
            <div class="jeeui-form-controls">
                <a href="#" class="jeeui-btn jeeui-btn-lg jeeui-btn-clear" title="清空"><i class="fa fa-clear"></i><span>清空</span></a>
            </div>
        </div>
        <div class="jeeui-form-group jeeui-form-button">
            <div class="jeeui-form-controls">
                <a href="#" class="jeeui-btn jeeui-btn-lg jeeui-btn-search" title="查询"><i class="fa fa-search"></i><span>查询</span></a>
            </div>
        </div>
    </form>
</div>

</@jee.override>

<@jee.override name="content">
<div class="jeeui-page-selected"></div>
<div class="jeeui-container"></div>
</@jee.override>

<@jee.extends name="/templates/default/contentPage.ftl"/>