<#assign scriptModuleName="base-page-chart">

<@jee.override name="content">

<@jee.block name="chart-list"></@jee.block>

<@jee.block name="chart-magnifier">
<div id="chartMagnifierDialog" style="display:none;">
    <a id="btnMagnifierDialogClose"><i class="fa fa-close"></i></a>
    <div id="chartMagnifier" style="width: 560px; height: 460px;"></div>
</div>
</@jee.block>

</@jee.override>

<@jee.extends name="/templates/default/contentPage.ftl"/>