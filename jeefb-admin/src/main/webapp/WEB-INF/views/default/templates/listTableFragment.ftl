<#if !noPaginate??><#assign noPaginate=false></#if>

<table border="0" cellpadding="0" cellspacing="0" class="jeeui-datagrid jeeui-datagrid-bordered jeeui-datagrid-striped jeeui-datagrid-hover">
    <thead>
        <@jee.block name="thead"></@jee.block>
    </thead>
    <tbody>
        <@jee.block name="tbody"></@jee.block>
    </tbody>
    <tfoot>
        <@jee.block name="tfoot"></@jee.block>
    </tfoot>
</table>

<#if !noPaginate>
<@component.paginate page />
</#if>