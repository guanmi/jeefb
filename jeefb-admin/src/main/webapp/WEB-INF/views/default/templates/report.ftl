<#assign pagecss="jeeui-page-report" />
<#if !scriptModuleName??><#assign scriptModuleName="page-list"></#if>

<@jee.override name="page-head">
<div class="jeeui-page-condition" style="${conditionDisplay!''}">
    <form id="report_form_id" class="jeeui-form-condition" method="post" enctype="multipart/form-data" role="form">
        <input type="hidden" name="content" id="content" value=""/>
        <@jee.block name="reportDiv"></@jee.block>
    </form>
</div>
</@jee.override>

<@jee.override name="page-toolbar">
<div class="jeeui-nav jeeui-nav-buttons">
    <@jee.block name="reportButtonDiv"></@jee.block>
</div>
</@jee.override>

<@jee.override name="content">
<div id="report_table_div" style="overflow:auto">
<@jee.block name="reportHtml"></@jee.block>
</div>
</@jee.override>

<@jee.extends name="/templates/default/contentPage.ftl"/>