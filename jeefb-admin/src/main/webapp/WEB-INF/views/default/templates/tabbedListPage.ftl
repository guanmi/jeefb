<#assign pagecss="jeeui-page-list" />
<#if !scriptModuleName??><#assign scriptModuleName="base-page-list"></#if>
<#if !useCondition??><#assign useCondition=true /></#if>

<#if !useCondition>
<#assign useConditionAttribute="style=\"padding: 0;border: 0;\"" />
</#if>

<@jee.override name="page-head">
<div class="jeeui-tabbed jeeui-tabbed-left" data-fit="false" data-bar-ignore="true">
    <div class="jeeui-tabbed-bar">
        <ul class="jeeui-tabbed-nav jeeui-nav jeeui-nav-tabs">
            <li class="jeeui-tabbed-label active">
                <a href="#pnlCommand"><i class="fa fa-cog"></i>命令</a>
            </li>
            <li class="jeeui-tabbed-label">
                <a href="#pnlCondition"><i class="fa fa-search"></i>查询</a>
            </li>
        </ul>
    </div>
    <div class="jeeui-tabbed-content">
        <div id="pnlCommand" class="jeeui-tabbed-panel active">
            <div class="jeeui-panel-body">
                <div class="jeeui-page-condition" ${useConditionAttribute}>
                    <form class="jeeui-form-condition jeeui-form-flow">
                        <@jee.block name="command"></@jee.block>
                    </form>
                </div>
            </div>
        </div>
        <div id="pnlCondition" class="jeeui-tabbed-panel">
            <div class="jeeui-panel-body">
                <div class="jeeui-page-condition" ${useConditionAttribute}>
                    <form class="jeeui-form-condition jeeui-form-flow">
                        <input type="hidden" class="jeeui-order-field" name="orderField" value=""/>
                        <input type="hidden" class="jeeui-order-direction" name="orderDirection" value="asc"/>
                        <input type="hidden" class="jeeui-paging-page" name="start" value="1"/>
                        <input type="hidden" class="jeeui-paging-size" name="limit" value="10"/>

                        <#if useCondition>

                        <@jee.block name="condition"></@jee.block>

                        <div class="jeeui-form-group jeeui-form-button">
                            <div class="jeeui-form-controls">
                                <a href="#" class="jeeui-btn jeeui-btn-lg jeeui-btn-search" title="查询"><i class="fa fa-search"></i><span>查询</span></a>
                            </div>
                        </div>
                        </#if>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@jee.override>

<@jee.override name="page-toolbar">
<div class="jeeui-btn-group jeeui-btn-group-lg">
    <@jee.block name="nav-buttons"></@jee.block>
</div>
<@jee.block name="page-toolbar-extend"></@jee.block>
</@jee.override>

<@jee.override name="content">
<div class="jeeui-gridview"></div>
</@jee.override>

<@jee.extends name="/templates/default/contentPage.ftl"/>