<#assign pagecss="jeeui-page-list" />
<#if !scriptModuleName??><#assign scriptModuleName="base-page-list"></#if>
<#if !useCondition??><#assign useCondition=true /></#if>

<#if !useCondition>
    <#assign useConditionAttribute="style=\"padding: 0;border: 0;\"" />
</#if>

<@jee.override name="page-head">
<div class="jeeui-page-condition" ${useConditionAttribute}>
    <form class="jeeui-form-condition jeeui-form-flow">
        <input type="hidden" class="jeeui-order-field" name="orderField" value=""/>
        <input type="hidden" class="jeeui-order-direction" name="orderDirection" value="asc"/>
        <input type="hidden" class="jeeui-paging-page" name="start" value="1"/>
        <input type="hidden" class="jeeui-paging-size" name="limit" value="10"/>
        
        <#if useCondition>
        
        <@jee.block name="condition"></@jee.block>
        
        <div class="jeeui-form-group jeeui-form-button">
            <div class="jeeui-form-controls">
                <a href="#" class="jeeui-btn jeeui-btn-lg jeeui-btn-search" title="查询"><i class="fa fa-search"></i><span>查询</span></a>
            </div>
        </div>
        </#if>
    </form>
</div>
</@jee.override>

<@jee.override name="page-toolbar">
<div class="jeeui-btn-group jeeui-btn-group-lg">
    <@jee.block name="nav-buttons"></@jee.block>
</div>
<@jee.block name="page-toolbar-extend"></@jee.block>
</@jee.override>

<@jee.override name="content">
<div class="jeeui-gridview"></div>
</@jee.override>

<@jee.extends name="/templates/default/contentPage.ftl"/>