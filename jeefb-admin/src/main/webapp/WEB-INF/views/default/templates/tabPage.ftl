<#if !scriptModuleName??><#assign scriptModuleName="page-tab"></#if>

<@jee.override name="page-toolbar">
<ul class="jeeui-tabbed-nav jeeui-nav jeeui-nav-tabs">
    <@jee.block name="tabs-label"></@jee.block>
</ul>
</@jee.override>

<@jee.override name="content">
<div class="jeeui-tabbed-content">
    <@jee.block name="tabs-content"></@jee.block>
</div>
</@jee.override>

<@jee.extends name="/templates/default/contentPage.ftl"/>