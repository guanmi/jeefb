<#if !scriptModuleName??><#assign scriptModuleName="page-edit-grid"></#if>
<#if !meltipartForm??><#assign meltipartForm=false></#if>

<@jee.override name="edit-body-extends">
<div class="jeeui-edit-grid">
    <div class="jeeui-edit-grid-toolbar">
        <@jee.block name="editgrid-toolbar"></@jee.block>
    </div>
    <div class="jeeui-edit-grid-body">
        <table border="0" cellpadding="0" cellspacing="0" class="jeeui-datagrid"></table>
    </div>
</div>
</@jee.override>

<@jee.extends name="/templates/default/editPage.ftl"/>