<#if !pageTitle??><#assign pageTitleAttribute="" /><#else>
<#assign pageTitleAttribute=" -- "+pageTitle /></#if> <#if !pageHeight??><#assign
pageHeightAttribute="100%" /><#else><#assign
pageHeightAttribute=pageHeight+"px" /></#if> <#if !pageWidth??><#assign
pageWidthAttribute="100%" /><#else><#assign
pageWidthAttribute=pageWidth+"px" /></#if> <#if !pageMax??> <#assign
pageMaxAttribute="false" /> <#else> <#assign pageHeightAttribute="100%"
/> <#assign pageWidthAttribute="100%" /> <#assign
pageMaxAttribute="true" /> </#if> <#if !scriptModuleName??><#assign
scriptModuleName="base-page-base"></#if> <@jee.project alias="project"/>

<!DOCTYPE html>
<html lang="zh-Hans">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />

<title>${project.projectName}${pageTitleAttribute}</title>

<link rel="stylesheet"
	href="${core.styleUrl}/lib/font-awesome-4.4.0.min.css" />
<link rel="stylesheet"
	href="${core.styleUrl}/lib/jquery.gritter-1.7.4.min.css" />
<link rel="stylesheet"
	href="${core.styleUrl}/lib/jquery.dataTables-1.10.9.min.css" />
<link rel="stylesheet" href="${core.styleUrl}/lib/ui-dialog-6.0.4.css" />
<link rel="stylesheet"
	href="${core.styleUrl}/lib/jquery.contextMenu-1.6.6.css" />
<link rel="stylesheet" href="${core.styleUrl}/lib/zTreeStyle-3.5.css" />
<link rel="stylesheet"
	href="${core.styleUrl}/lib/jquery.fancybox-2.1.5.css" />
<link rel="stylesheet" href="${core.styleUrl}/framework.min.css" />
<link rel="stylesheet" href="${core.themeStyleUrlHome}/theme.min.css" />
<link rel="stylesheet" href="${core.skinStyleUrlHome}/skin.min.css" />
<link rel="stylesheet" href="${core.themeStyleUrl}/theme.min.css" />
<link rel="stylesheet" href="${core.skinStyleUrl}/skin.min.css" />
<script src="${core.scriptUrl}/lib/modernizr-2.8.3.min.js"></script>
<script src="${core.skinScriptUrlHome}/defaults.js"></script>
<@jee.block name="head"></@jee.block>
</head>
<body style="height: ${pageHeightAttribute"
	data-max="${pageMaxAttribute}">
	<div id="loading-screen" class="jeeui-loading">
		<div class="jeeui-loading-mask"></div>
		<div class="jeeui-loading-container">
			<div class="jeeui-loading-indicator">
				<img src="${core.themeImageUrl}/loading.gif" />

				<p class="jeeui-loading-msg">加载资源中,请稍候...</p>
			</div>
		</div>
	</div>
	<@jee.block name="body"></@jee.block>
	<script>
		var PAGE_MODULE_NAME = '${scriptModuleName}';
		var AUTO_RESIZE = true;
	</script>
	<@jee.block name="scripts"></@jee.block>
	<script data-main="${core.scriptUrl}/main"
		src="${core.scriptUrl}/lib/require-2.1.20.min.js"></script>
</body>
</html>