<#if !pagecss??><#assign pagecss="" /></#if>
<#if !scriptModuleName??><#assign scriptModuleName="page-content"></#if>
<#if !useBreadcrumbs??><#assign useBreadcrumbs=true /></#if>
<#if !useToolbar??><#assign useToolbar=true /></#if>

<@jee.override name="body">
<div class="jeeui-page ${pagecss}">
    <div class="jeeui-page-head">
        <#if useBreadcrumbs>
        <div class="jeeui-breadcrumbs">
            <@jee.breadcrumb alias="breadcrumbItems" uri="${PAGE_URI}"/>
            <@component.breadcrumb items=breadcrumbItems />
        </div>
        </#if>
        <#if useToolbar>
        <div class="jeeui-page-toolbar">
            <@jee.block name="page-toolbar"></@jee.block>
        </div>
        </#if>
        <@jee.block name="page-head"></@jee.block>
    </div>
    <div class="jeeui-page-body">
        <@jee.block name="content">
        <div class="jeeui-container"></div>
        </@jee.block>
    </div>
</div>
</@jee.override>

<@jee.extends name="/templates/default/page.ftl"/>