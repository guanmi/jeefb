<#assign pageHeight="400" />
<#assign pageWidth="800" />
<#assign scriptModuleName="page-base">

<@jee.override name="body">
<div class="jeeui-page">
    <div class="jeeui-page-body">
        <@jee.block name="content">
        <div class="jeeui-container"></div>
        </@jee.block>
    </div>
</div>
</@jee.override>

<@jee.extends name="/templates/default/page.ftl"/>