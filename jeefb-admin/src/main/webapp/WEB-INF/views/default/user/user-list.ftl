<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><@jeefb.message code="common.platform"/></title>
<meta name="keywords" content="JAVA开源电商,SpringMVC,Bootstrap,CMS,响应式后台">
<meta name="description"
	content="JAVA开源电商,SpringMVC,Bootstrap,CMS,基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link href="${core.scriptUrl}/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${core.scriptUrl}/plugins/bootstrap-table/bootstrap-table.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${core.scriptUrl}/plugins/x-editable/css/bootstrap-editable.css">
<link rel="stylesheet" href="${core.cssUrl}/jeefb-table.css">
<script src="${core.scriptUrl}/jquery-2.1.1.min.js"></script>
<script src="${core.scriptUrl}/plugins/bootstrap/js/bootstrap.js"></script>
<script
	src="${core.scriptUrl}/plugins/bootstrap-table/bootstrap-table.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="${core.scriptUrl}/html5shiv.min.js"></script>
  <script src="${core.scriptUrl}/respond.min.js"></script>
  <script src="${core.scriptUrl}/json2.min.js"></script>
  <![endif]-->
<script
	src="${core.scriptUrl}/plugins/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<script
	src="${core.scriptUrl}/plugins/bootstrap-table/extensions/export/tableExport.js"></script>
<script
	src="${core.scriptUrl}/plugins/bootstrap-table/extensions/editable/bootstrap-table-editable.js"></script>
<script
	src="${core.scriptUrl}/plugins/x-editable/js/bootstrap-editable.js"></script>
<script
	src="${core.scriptUrl}/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
</head>
<body>
	<div class="container">
		<div id="toolbar">
			<button id="add" class="btn btn-primary">
				<i class="glyphicon glyphicon-save"></i> 增加
			</button>
			<button id="update" class="btn btn-success">
				<i class="glyphicon glyphicon-tag"></i> 修改
			</button>
			<button id="remove" class="btn btn-danger">
				<i class="glyphicon glyphicon-remove"></i> 删除
			</button>
			
				
			<div >
			用户名<input type="text" id="userName" name="userName" />
			邮箱 <input type="text" id="email" name="email"  />
				状态：<select id="status" name="status" ></select>
			</div>
		</div>
		<table id="table" data-toolbar="#toolbar" data-show-refresh="true"
			data-show-toggle="true" data-show-columns="true"
			data-show-export="true" data-minimum-count-columns="2"
			data-show-pagination-switch="true" data-pagination="true"
			data-query-params="queryParams" data-id-field="id"
			data-page-list="[10, 25, 50, 100, ALL]" data-show-footer="false"
			data-side-pagination="server" data-url="${core.base}/user/list"
			data-response-handler="responseHandler">
		</table>
	</div>

	<script>
		var $table = $('#table'), $remove = $('#remove'), $add = $('#add'), $update = $('#update'), selections = [];

		function initTable() {
			$table.bootstrapTable({
				height : getHeight(),
				columns : [ {
					field : 'state',
					checkbox : true,
					align : 'center',
					valign : 'middle'
				}, {
					title : '用户编码',
					field : 'id',
					align : 'center',
					valign : 'middle',
					sortable : true,
					footerFormatter : totalTextFormatter
				}, {
					field : 'userName',
					title : '用户名',
					sortable : true,
					footerFormatter : totalNameFormatter,
					align : 'center'
				}, {
					field : 'realName',
					title : '真实姓名',
					sortable : true,
					align : 'center',
					footerFormatter : totalNameFormatter
				}, {
					field : 'phone',
					title : '手机号',
					sortable : true,
					align : 'center',
					footerFormatter : totalNameFormatter
				}, {
					field : 'email',
					title : '邮箱',
					sortable : true,
					align : 'center',
					footerFormatter : totalNameFormatter
				}, {
					field : 'status',
					title : '状态',
					sortable : true,
					align : 'center',
					footerFormatter : totalNameFormatter
				}, {
					field : 'createDate',
					title : '创建时间',
					sortable : true,
					align : 'center',
					footerFormatter : totalNameFormatter
				}, {
					field : 'operate',
					title : '操作',
					align : 'center',
					events : operateEvents,
					formatter : operateFormatter
				} ]
			});
			// sometimes footer render error.
			setTimeout(function() {
				$table.bootstrapTable('resetView');
			}, 200);
			$table.on('check.bs.table uncheck.bs.table '
					+ 'check-all.bs.table uncheck-all.bs.table', function() {
				selections = getIdSelections();
			});
			$table.on('expand-row.bs.table', function(e, index, row, $detail) {
				if (index % 2 == 1) {
					$detail.html('Loading from ajax request...');
					$.get('LICENSE', function(res) {
						$detail.html(res.replace(/\n/g, '<br>'));
					});
				}
			});
			$table.on('all.bs.table', function(e, name, args) {
				console.log(name, args);
			});
			//批量删除功能
			$remove.click(function() {
				var ids = getIdSelections();
				$.ajax({
					type : "POST",
					url : "${core.base}/user/deteleBatch",
					data : {
						"ids" : ids
					},
					dataType : "json",
					success : function(data) {
						if (data.success == true) {
							alert(data.msg);
							$table.bootstrapTable('remove', {
								field : 'id',
								values : ids
							});
						} else {
							alert(data.msg);
						}
					}
				});
			});

			$add.click(function() {
				//打开保存窗口

			});

			$update.click(function() {
				var ids = getIdSelections();
				$.ajax({
					type : "POST",
					url : "${core.base}/user/deteleBatch",
					data : {
						"ids" : ids
					},
					dataType : "json",
					success : function(data) {
						if (data.success == true) {
							alert(data.msg);
							$table.bootstrapTable('remove', {
								field : 'id',
								values : ids
							});
						} else {
							alert(data.msg);
						}
					}
				});
			});
			$(window).resize(function() {
				$table.bootstrapTable('resetView', {
					height : getHeight()
				});
			});
		}

		function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function(row) {
				return row.id
			});
		}

		function responseHandler(res) {
			$.each(res.rows, function(i, row) {
				row.state = $.inArray(row.id, selections) !== -1;
			});
			return res;
		}

		function operateFormatter(value, row, index) {
			return [
					'<a class="like" href="javascript:void(0)" title="Like">',
					'<i class="glyphicon glyphicon-heart"></i>',
					'</a>  ',
					'<a class="remove" href="javascript:void(0)" title="Remove">',
					'<i class="glyphicon glyphicon-remove"></i>', '</a>' ]
					.join('');
		}

		window.operateEvents = {
			'click .like' : function(e, value, row, index) {
				alert('You click like action, row: ' + JSON.stringify(row));
			},
			'click .remove' : function(e, value, row, index) {
				$.ajax({
					type : "POST",
					url : "${core.base}/user/detele",
					data : {
						"id" : row.id
					},
					dataType : "json",
					success : function(data) {
						if (data.success == true) {
							alert(data.msg);
							$table.bootstrapTable('remove', {
								field : 'id',
								values : [ row.id ]
							});
						} else {
							alert(data.msg);
						}
					}
				});
			}
		};

		function totalTextFormatter(data) {
			return 'Total';
		}

		function totalNameFormatter(data) {
			return data.length;
		}

		function totalPriceFormatter(data) {
			var total = 0;
			$.each(data, function(i, row) {
				total += +(row.price.substring(1));
			});
			return '$' + total;
		}

		function getHeight() {
			return $(window).height() - $('h1').outerHeight(true);
		}

		$(function() {
			initTable();
		});

		function queryParams(params) {
			return {
				limit : params.limit,
				offset : params.offset,
				userName : params.search,
				sort : params.sort,
				order : params.order
			};
		}
	</script>
</body>
</html>