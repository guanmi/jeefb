<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><@jeefb.message code="common.platform"/></title>
    <link href="${core.scriptUrl}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${core.themeBase}/nifty/css/nifty.min.css" rel="stylesheet">
    <link href="${core.scriptUrl}/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="${core.themeBase}/nifty/css/jeefb/nifty-jeefb.min.css" rel="stylesheet">
    <link href="${core.scriptUrl}/plugins/pace/pace.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${core.scriptUrl}/plugins/form-validation/formValidation.css"/>
    <link rel="stylesheet" href="${core.cssUrl}/tipsy.css"/>
</head>
<body>
   <div id="messages"></div>
   <div id="successLogin"></div>
   <div class="text_success"> </div>
   
	<div id="container" class="cls-container">
		<div id="bg-overlay" class="bg-img img-balloon"></div>
		<div class="cls-header cls-header-lg">
			<div class="cls-brand">
				<a class="box-inline" href="${core.themeBase}/nifty/index.html">
					<span class="brand-title"><@jeefb.message code="common.platform"/></span>
				</a>
			</div>
		</div>
		<div class="cls-content">
			<div class="cls-content-sm panel">
				<div class="panel-body">
					<p class="pad-btm"><@jeefb.message code="login.please"/></p>
					<form  id="formLogin" action="${core.base}/login" method="post"  check="${core.base}/checkuser">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-user"></i></div>
								<input type="text" id="userName" name="userName" class="form-control" placeholder="<@jeefb.message code="common.userName"/>">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
								<input type="password" id="password" name="password" class="form-control" placeholder="<@jeefb.message code="common.password"/>">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 ">
								<input type="text" id="verfiyCode" name="verfiyCode"  class="col-xs-6 col-xs-offset-1" placeholder="<@jeefb.message code="login.verfiyCode"/>">
								<img  id="randCodeImage" src="${core.base}/servlet/validateCodeServlet" class="width-40" onclick="this.src=('${core.base}/servlet/validateCodeServlet?'+(new Date()).getTime())"/>
								<button class="btn btn-primary  fa fa-repeat" id="randCodeImage"></button>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-12">
								<label class="form-checkbox form-icon ">
									<input type="checkbox" id="on_off"> <@jeefb.message code="login.remember"/>
								</label>
								<button class="btn btn-info btn-labeled fa fa-user fa-lg"  id="but_login"><@jeefb.message code="login.login"/></button>
								<button class="btn btn-warning btn-labeled fa fa-repeat fa-lg" type="reset" id="resetFrom"><@jeefb.message code="common.reset"/></button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="pad-ver">
				<a href="${core.themeBase}/nifty/pages-password-reminder.html" class="btn-link mar-rgt"><@jeefb.message code="login.forgot.password"/></a>
				<a href="${core.themeBase}/nifty/pages-register.html" class="btn-link mar-lft"><@jeefb.message code="login.register"/></a>
			</div>
		</div>
		<div class="jeefb-bg">
			<div id="jeefb-bg-list">
				<div class="jeefb-loading"><i class="fa fa-refresh"></i></div>
				<img class="jeefb-chg-bg bg-trans" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-trns.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-1.jpg" alt="Background Image">
				<img class="jeefb-chg-bg active" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-2.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-3.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-4.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-5.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-6.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-7.jpg" alt="Background Image">
			</div>
		</div>
	</div>
    <script src="${core.scriptUrl}/jquery-2.1.1.min.js"></script>
    <script src="${core.themeBase}/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="${core.scriptUrl}/plugins/fast-click/fastclick.min.js"></script>
    <script src="${core.themeBase}/nifty/js/nifty.min.js"></script>
    <script src="${core.themeBase}/nifty/js/jeefb/bg-images.js"></script>
    <script src="${core.scriptUrl}/plugins/pace/pace.min.js"></script>
    <script type="text/javascript" src="${core.scriptUrl}/jquery.cookie.js"></script>
    <script type="text/javascript" src="${core.scriptUrl}/plugins/form-validation/formValidation.js"></script>
    <script type="text/javascript" src="${core.scriptUrl}/plugins/form-validation/language/zh_CN.js"></script>
    <script type="text/javascript" src="${core.scriptUrl}/lang/zh_CN.js"></script>
    <script type="text/javascript" src="${core.scriptUrl}/login.js"></script>
    <script type="text/javascript" src="${core.scriptUrl}/jquery.tipsy.js"></script>
</body>
</html>
