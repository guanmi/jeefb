<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="nav-close">
		<i class="fa fa-times-circle"></i>
	</div>
	<div class="slimScrollDiv"
		style="position: relative; width: auto; height: 100%;">
		<div class="sidebar-collapse" style="width: auto; height: 100%;">
			<ul class="nav" id="side-menu">
				<li class="nav-header">
					<div class="dropdown profile-element">
						<span><img alt="image" class="img-circle"
							src="${core.imageUrl}/profile_small.jpg"></span> <a
							data-toggle="dropdown" class="dropdown-toggle"
							href="#"> <span
							class="clear"> <span class="block m-t-xs"><strong
									class="font-bold">${user.userName!""}</strong></span> <span
								class="text-muted text-xs block">${user.realName!""}<b
									class="caret"></b></span>
						</span>
						</a>
						<ul class="dropdown-menu animated fadeInRight m-t-xs">
							<li><a class="J_menuItem"
								href="#"
								data-index="0">修改头像</a></li>
							<li><a class="J_menuItem"
								href="#"
								data-index="1">个人资料</a></li>
							<li><a class="J_menuItem"
								href="#"
								data-index="2">联系我们</a></li>
							<li><a class="J_menuItem"
								href="#" data-index="3">信箱</a>
							</li>
							<li class="divider"></li>
							<li><a href="${core.base}/logout">安全退出</a>
							</li>
						</ul>
					</div>
					<div class="logo-element"></div>
				</li> 
				
				
				
				
				<ul class="mainmenu nav nav-list">
				
					
				<#list functionList as function>
				<li class="active"><a href="${core.base}/${function.functionUrl!''}"> <i
						class="fa fa-home"></i> <span class="nav-label"><@jeefb.message code="${function.functionName}"/></span> <span
						class="fa arrow">
						</span>
					</a>
				<#if function.childrens?size gt 0>
					<ul class="nav nav-second-level collapse">
					  <#list function.childrens as menu> 
						<li><a class="J_menuItem"
							href="${core.base}${menu.functionUrl!''}"
							data-index="${function.id}"> <@jeefb.message code="${menu.functionName}"/></a>
					   </li>
					  </#list> 
					</ul>
				</#if>	
				</li> 
				</#list>
	

			</ul>
		</div>
		<div class="slimScrollBar"
			style="width: 4px; position: absolute; top: 179px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 358.492px; background: rgb(0, 0, 0);"></div>
		<div class="slimScrollRail"
			style="width: 4px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.9; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div>
	</div>
</nav>