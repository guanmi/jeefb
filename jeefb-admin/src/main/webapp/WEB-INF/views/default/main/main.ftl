<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp">
<title><@jeefb.message code="common.platform"/></title>
<meta name="keywords" content="JAVA开源电商,SpringMVC,Bootstrap,CMS,响应式后台" />
<meta name="description" content="JAVA开源电商,SpringMVC,Bootstrap,CMS,基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术" />
<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<link href="${core.scriptUrl}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="${core.scriptUrl}/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">   
<link href="${core.cssUrl}/animate-css/animate.min.css" rel="stylesheet"/>
<link href="${core.themeBase}/default/css/style.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="${core.themeBase}/default/css/style.css" />
<link rel="stylesheet" href="${core.scriptUrl}/plugins/layer/skin/layer.css"/>
<link rel="stylesheet" href="${core.scriptUrl}/plugins/layer/skin/layer.ext.css" />
</head>

<body class="fixed-sidebar full-height-layout gray-bg  pace-done"
	style="overflow: hidden">
	JEEFB
</body>
</html>