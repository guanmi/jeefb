<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp">
<title><@jeefb.message code="common.platform"/></title>
<meta name="keywords" content="JAVA开源电商,SpringMVC,Bootstrap,CMS,响应式后台" />
<meta name="description" content="JAVA开源电商,SpringMVC,Bootstrap,CMS,基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术" />
<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<link href="${core.scriptUrl}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="${core.scriptUrl}/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">   
<link href="${core.cssUrl}/animate-css/animate.min.css" rel="stylesheet"/>
<link href="${core.themeBase}/default/css/style.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="${core.themeBase}/default/css/style.css" />
<link rel="stylesheet" href="${core.scriptUrl}/plugins/layer/skin/layer.css"/>
<link rel="stylesheet" href="${core.scriptUrl}/plugins/layer/skin/layer.ext.css" />

</head>

<body class="fixed-sidebar full-height-layout gray-bg  pace-done"
	style="overflow: hidden">
	<div class="pace  pace-inactive">
		<div class="pace-progress" data-progress-text="100%"
			data-progress="99" style="width: 100%;">
			<div class="pace-progress-inner"></div>
		</div>
		<div class="pace-activity"></div>
	</div>
	<div id="wrapper">
		<!--左侧导航开始-->
		<#include "menu.ftl">
		<!--左侧导航结束-->
		<!--右侧部分开始-->
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<#include "top.ftl">
			<div class="row content-tabs">
				<button class="roll-nav roll-left J_tabLeft">
					<i class="fa fa-backward"></i>
				</button>
				<nav class="page-tabs J_menuTabs">
					<div class="page-tabs-content">
						<a href="javascript:;" class="J_menuTab" data-id="index_v1.html">首页</a>
					</div>
				</nav>
				<button class="roll-nav roll-right J_tabRight">
					<i class="fa fa-forward"></i>
				</button>
				<div class="btn-group roll-nav roll-right">
					<button class="dropdown J_tabClose" data-toggle="dropdown">
						关闭操作<span class="caret"></span>

					</button>
					<ul role="menu" class="dropdown-menu dropdown-menu-right">
						<li class="J_tabShowActive"><a>定位当前选项卡</a></li>
						<li class="divider"></li>
						<li class="J_tabCloseAll"><a>关闭全部选项卡</a></li>
						<li class="J_tabCloseOther"><a>关闭其他选项卡</a></li>
					</ul>
				</div>
				<a href="${core.base}/logout"
					class="roll-nav roll-right J_tabExit"><i
					class="fa fa fa-sign-out"></i> 退出</a>
			</div>
			<div class="row J_mainContent" id="content-main">
				<iframe class="J_iframe" name="iframe0" width="100%" height="100%"
					src="${core.base}/system/main" frameborder="0"
					data-id="widgets.html" seamless=""></iframe>
			</div>
			<div class="footer">
				<div class="pull-right">
					© 2015-2016 <a href="#" target="_blank">guanmi</a>
				</div>
			</div>
		</div>
		<!--右侧部分结束-->
		<!--右侧边栏开始-->
		<div id="right-sidebar">
			<div class="slimScrollDiv"
				style="position: relative; width: auto; height: 100%;">
				<div class="sidebar-container" style="width: auto; height: 100%;">

					<ul class="nav nav-tabs navs-3">

						<li class="active"><a data-toggle="tab"
							href="#"> <i
								class="fa fa-gear"></i> 主题
						</a></li>
						<li class=""><a data-toggle="tab"
							href="#"> 通知 </a></li>
						<li><a data-toggle="tab"
							href="#"> 项目进度 </a></li>
					</ul>

					<div class="tab-content">
						<div id="tab-1" class="tab-pane active">
							<div class="sidebar-title">
								<h3>
									<i class="fa fa-comments-o"></i> 主题设置
								</h3>
								<small><i class="fa fa-tim"></i>
									你可以从这里选择和预览主题的布局和样式，这些设置会被保存在本地，下次打开的时候会直接应用这些设置。</small>
							</div>
							<div class="skin-setttings">
								<div class="title">主题设置</div>
								<div class="setings-item">
									<span>收起左侧菜单</span>
									<div class="switch">
										<div class="onoffswitch">
											<input type="checkbox" name="collapsemenu"
												class="onoffswitch-checkbox" id="collapsemenu"> <label
												class="onoffswitch-label" for="collapsemenu"> <span
												class="onoffswitch-inner"></span> <span
												class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</div>
								<div class="setings-item">
									<span>固定顶部</span>

									<div class="switch">
										<div class="onoffswitch">
											<input type="checkbox" name="fixednavbar"
												class="onoffswitch-checkbox" id="fixednavbar"> <label
												class="onoffswitch-label" for="fixednavbar"> <span
												class="onoffswitch-inner"></span> <span
												class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</div>
								<div class="setings-item">
									<span> 固定宽度 </span>

									<div class="switch">
										<div class="onoffswitch">
											<input type="checkbox" name="boxedlayout"
												class="onoffswitch-checkbox" id="boxedlayout"> <label
												class="onoffswitch-label" for="boxedlayout"> <span
												class="onoffswitch-inner"></span> <span
												class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</div>
								<div class="title">皮肤选择</div>
								<div class="setings-item default-skin nb">
									<span class="skin-name "> <a
										href="#" class="s-skin-0">
											默认皮肤 </a>
									</span>
								</div>
								<div class="setings-item blue-skin nb">
									<span class="skin-name "> <a
										href="#" class="s-skin-1">
											蓝色主题 </a>
									</span>
								</div>
								<div class="setings-item yellow-skin nb">
									<span class="skin-name "> <a
										href="#" class="s-skin-3">
											黄色/紫色主题 </a>
									</span>
								</div>
							</div>
						</div>
						<div id="tab-2" class="tab-pane">

							<div class="sidebar-title">
								<h3>
									<i class="fa fa-comments-o"></i> 最新通知
								</h3>
								<small><i class="fa fa-tim"></i> 您当前有10条未读信息</small>
							</div>

							<div>

								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a1.jpg">

											<div class="m-t-xs">
												<i class="fa fa-star text-warning"></i> <i
													class="fa fa-star text-warning"></i>
											</div>
										</div>
										<div class="media-body">

											据天津日报报道：瑞海公司董事长于学伟，副董事长董社轩等10人在13日上午已被控制。 <br> <small
												class="text-muted">今天 4:21</small>
										</div>
									</a>
								</div>
								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a2.jpg">
										</div>
										<div class="media-body">
											HCY48之音乐大魔王会员专属皮肤已上线，快来一键换装拥有他，宣告你对华晨宇的爱吧！ <br> <small
												class="text-muted">昨天 2:45</small>
										</div>
									</a>
								</div>
								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a3.jpg">

											<div class="m-t-xs">
												<i class="fa fa-star text-warning"></i> <i
													class="fa fa-star text-warning"></i> <i
													class="fa fa-star text-warning"></i>
											</div>
										</div>
										<div class="media-body">
											写的好！与您分享 <br> <small class="text-muted">昨天 1:10</small>
										</div>
									</a>
								</div>
								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a4.jpg">
										</div>

										<div class="media-body">
											国外极限小子的炼成！这还是亲生的吗！！ <br> <small class="text-muted">昨天
												8:37</small>
										</div>
									</a>
								</div>
								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a8.jpg">
										</div>
										<div class="media-body">

											一只流浪狗被收留后，为了减轻主人的负担，坚持自己觅食，甚至......有些东西，可能她比我们更懂。 <br> <small
												class="text-muted">今天 4:21</small>
										</div>
									</a>
								</div>
								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a7.jpg">
										</div>
										<div class="media-body">
											这哥们的新视频又来了，创意杠杠滴，帅炸了！ <br> <small class="text-muted">昨天
												2:45</small>
										</div>
									</a>
								</div>
								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a3.jpg">

											<div class="m-t-xs">
												<i class="fa fa-star text-warning"></i> <i
													class="fa fa-star text-warning"></i> <i
													class="fa fa-star text-warning"></i>
											</div>
										</div>
										<div class="media-body">
											最近在补追此剧，特别喜欢这段表白。 <br> <small class="text-muted">昨天
												1:10</small>
										</div>
									</a>
								</div>
								<div class="sidebar-message">
									<a href="#">
										<div class="pull-left text-center">
											<img alt="image" class="img-circle message-avatar"
												src="${core.imageUrl}/a4.jpg">
										</div>
										<div class="media-body">
											我发起了一个投票 【你认为下午大盘会翻红吗？】 <br> <small class="text-muted">星期一
												8:37</small>
										</div>
									</a>
								</div>
							</div>

						</div>
						<div id="tab-3" class="tab-pane">

							<div class="sidebar-title">
								<h3>
									<i class="fa fa-cube"></i> 最新任务
								</h3>
								<small><i class="fa fa-tim"></i> 您当前有14个任务，10个已完成</small>
							</div>

							<ul class="sidebar-list">
								<li><a href="#">
										<div class="small pull-right m-t-xs">9小时以后</div>
										<h4>市场调研</h4> 按要求接收教材；

										<div class="small">已完成： 22%</div>
										<div class="progress progress-mini">
											<div style="width: 22%;"
												class="progress-bar progress-bar-warning"></div>
										</div>
										<div class="small text-muted m-t-xs">项目截止： 4:00 -
											2015.10.01</div>
								</a></li>
								<li><a href="#">
										<div class="small pull-right m-t-xs">9小时以后</div>
										<h4>可行性报告研究报上级批准</h4> 编写目的编写本项目进度报告的目的在于更好的控制软件开发的时间,对团队成员的
										开发进度作出一个合理的比对

										<div class="small">已完成： 48%</div>
										<div class="progress progress-mini">
											<div style="width: 48%;" class="progress-bar"></div>
										</div>
								</a></li>
								<li><a href="#">
										<div class="small pull-right m-t-xs">9小时以后</div>
										<h4>立项阶段</h4> 东风商用车公司 采购综合综合查询分析系统项目进度阶段性报告武汉斯迪克科技有限公司

										<div class="small">已完成： 14%</div>
										<div class="progress progress-mini">
											<div style="width: 14%;"
												class="progress-bar progress-bar-info"></div>
										</div>
								</a></li>
								<li><a href="#"> <span
										class="label label-primary pull-right">NEW</span>
										<h4>设计阶段</h4> <!--<div class="small pull-right m-t-xs">9小时以后</div>-->
										项目进度报告(Project Progress Report)
										<div class="small">已完成： 22%</div>
										<div class="small text-muted m-t-xs">项目截止： 4:00 -
											2015.10.01</div>
								</a></li>
								<li><a href="#">
										<div class="small pull-right m-t-xs">9小时以后</div>
										<h4>拆迁阶段</h4> 科研项目研究进展报告 项目编号: 项目名称: 项目负责人:

										<div class="small">已完成： 22%</div>
										<div class="progress progress-mini">
											<div style="width: 22%;"
												class="progress-bar progress-bar-warning"></div>
										</div>
										<div class="small text-muted m-t-xs">项目截止： 4:00 -
											2015.10.01</div>
								</a></li>
								<li><a href="#">
										<div class="small pull-right m-t-xs">9小时以后</div>
										<h4>建设阶段</h4> 编写目的编写本项目进度报告的目的在于更好的控制软件开发的时间,对团队成员的
										开发进度作出一个合理的比对

										<div class="small">已完成： 48%</div>
										<div class="progress progress-mini">
											<div style="width: 48%;" class="progress-bar"></div>
										</div>
								</a></li>
								<li><a href="#">
										<div class="small pull-right m-t-xs">9小时以后</div>
										<h4>获证开盘</h4> 编写目的编写本项目进度报告的目的在于更好的控制软件开发的时间,对团队成员的
										开发进度作出一个合理的比对

										<div class="small">已完成： 14%</div>
										<div class="progress progress-mini">
											<div style="width: 14%;"
												class="progress-bar progress-bar-info"></div>
										</div>
								</a></li>

							</ul>

						</div>
					</div>

				</div>
				<div class="slimScrollBar"
					style="width: 4px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 606px; background: rgb(0, 0, 0);"></div>
				<div class="slimScrollRail"
					style="width: 4px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.4; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div>
			</div>
		</div>
		<!--右侧边栏结束-->
		
		
	</div>
	<script src="${core.scriptUrl}/jquery-2.1.1.min.js"></script>
	 <script src="${core.scriptUrl}/plugins/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="${core.scriptUrl}/jquery.metisMenu.js"></script>
	<script type="text/javascript" src="${core.scriptUrl}/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="${core.scriptUrl}/plugins/layer/layer.min.js"></script>
	<script type="text/javascript" src="${core.themeBase}/default/js/default.js"></script>
	<script type="text/javascript" src="${core.themeBase}/default/js/contabs.min.js"></script>
	<script type="text/javascript" src="${core.scriptUrl}/plugins/pace/pace.min.js"></script>
</body>
</html>