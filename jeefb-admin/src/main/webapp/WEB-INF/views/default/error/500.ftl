<html>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><@jeefb.message code="common.platform"/></title>
<meta name="keywords" content="JAVA开源电商,SpringMVC,Bootstrap,CMS,响应式后台" />
<meta name="description" content="JAVA开源电商,SpringMVC,Bootstrap,CMS,基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术" />
<link href="${core.themeBase}/default/css/bootstrap.min.css" rel="stylesheet">
<link href="${core.themeBase}/default/css/font-awesome.min.css" rel="stylesheet">
<link href="${core.themeBase}/default/css/animate.min.css" rel="stylesheet">
<link href="${core.themeBase}/default/css/style.min.css" rel="stylesheet">
</head>
<body class="gray-bg">
	<div class="middle-box text-center animated fadeInDown">
		<h1>500</h1>
		<h3 class="font-bold">服务器内部错误</h3>

		<div class="error-desc">
			服务器好像出错了... <br />您可以返回主页看看 <br />
			<a href="index.html" tppabs="/index.html" class="btn btn-primary m-t">主页</a>
		</div>
	</div>
	<script src="${core.themeBase}/default/js/jquery.min.js"></script>
	<script src="${core.themeBase}/default/js/bootstrap.min.js"></script>
</body>
</html>
