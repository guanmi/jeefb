<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>登录 -<@jeefb.message code="common.platform"/></title>
<meta name="keywords" content="JEE FAST BOOT" />
<meta name="description" content="CMS,微信,电商" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- basic styles -->
<link href="${core.themeBase}/ace/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="${core.themeBase}/ace/css/font-awesome.min.css" />
<!--[if IE 7]>
		  <link rel="stylesheet" href="${core.themeBase}/ace/css/font-awesome-ie7.min.css" />
<![endif]-->
<!-- ace styles -->
<link rel="stylesheet" href="${core.themeBase}/ace/css/ace.min.css" />
<link rel="stylesheet" href="${core.themeBase}/ace/css/ace-rtl.min.css" />
<!--[if lte IE 8]>
		  <link rel="stylesheet" href="${core.themeBase}/ace/css/ace-ie.min.css" />
<![endif]-->
<!--[if lt IE 9]>
		<script src="${core.themeBase}/ace/js/html5shiv.js"></script>
		<script src="${core.themeBase}/ace/js/respond.min.js"></script>
<![endif]-->
</head>
<body class="login-layout">
	<div class="main-container">
		<div class="main-content">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="login-container">
						<div class="center">
							<h1>
								<i class="icon-leaf green"></i>  <span
									class="white"><@jeefb.message code="common.platform"/></span>
							</h1>
							<h4 class="blue"></h4>
						</div>

						<div class="space-6"></div>

						<div class="position-relative">
							<div id="login-box"
								class="login-box visible widget-box no-border">
								<div class="widget-body">
									<div class="widget-main">
										<h4 class="header blue lighter bigger">
											<i class="icon-coffee green"></i> <@jeefb.message code="login.please"/>
										</h4>
										<div class="space-6"></div>
										<form action="${core.base}/login">
											<fieldset>
												<label class="block clearfix"> <span
													class="block input-icon input-icon-right"> <input
														type="text" class="form-control" placeholder="<@jeefb.message code="common.userName"/>" />
														<i class="icon-user"></i>
												</span>
												</label> <label class="block clearfix"> <span
													class="block input-icon input-icon-right"> <input
														type="password" class="form-control"
														placeholder='<@jeefb.message code="common.password"/>' /> <i class="icon-lock"></i>
												</span>
												</label>
												<div class="clearfix">
													<label class="inline"> <input
														type="input" class="form-control"
														placeholder="<@jeefb.message code="login.verfiyCode"/>" /> 
													</label>
                                                     
													<img  src="${core.base}/servlet/validateCodeServlet" class="width-35 pull-right "/>
													</button>
												</div>
												<div class="space"></div>

												<div class="clearfix">
													<label class="inline"> <input type="checkbox"
														class="ace" /> <span class="lbl"><@jeefb.message code="login.remember"/></span>
													</label>

													<button type="submit" 
														class="width-35 pull-right btn btn-sm btn-primary">
														<i class="icon-key"></i> <@jeefb.message code="login.login"/>
													</button>
												</div>

												<div class="space-4"></div>
											</fieldset>
										</form>
									</div>

									<div class="toolbar clearfix">
										<div>
											<a href="#" onclick="show_box('forgot-box'); return false;"
												class="forgot-password-link"> <i class="icon-arrow-left"></i>
												<@jeefb.message code="login.forgot.password"/>
											</a>
										</div>

										<div>
											<a href="#" onclick="show_box('signup-box'); return false;"
												class="user-signup-link"> 
												<@jeefb.message code="login.register"/>
												 <i
												class="icon-arrow-right"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div id="forgot-box" class="forgot-box widget-box no-border">
								<div class="widget-body">
									<div class="widget-main">
										<h4 class="header red lighter bigger">
											<i class="icon-key"></i> 
											<@jeefb.message code="login.retrieve.password"/>
										</h4>

										<div class="space-6"></div>
										<p><@jeefb.message code="login.email.receive.instructions"/>
										</p>
										<form>
											<fieldset>
												<label class="block clearfix"> <span
													class="block input-icon input-icon-right"> <input
														type="email" class="form-control" placeholder='<@jeefb.message code="common.email"/>' />
														<i class="icon-envelope"></i>
												</span>
												</label>

												<div class="clearfix">
													<button type="button"
														class="width-35 pull-right btn btn-sm btn-danger">
														<i class="icon-lightbulb"></i> 
														<@jeefb.message code="common.send.me"/>
													</button>
												</div>
											</fieldset>
										</form>
									</div>
									<div class="toolbar center">
										<a href="#" onclick="show_box('login-box'); return false;"
											class="back-to-login-link"> 
											<@jeefb.message code="login.login"/>
											 <i
											class="icon-arrow-right"></i>
										</a>
									</div>
								</div>
							</div>

							<div id="signup-box" class="signup-box widget-box no-border">
								<div class="widget-body">
									<div class="widget-main">
										<h4 class="header green lighter bigger">
											<i class="icon-group blue"></i>
											<@jeefb.message code="user.new.register"/>
										</h4>

										<div class="space-6"></div>
										<p>
										<@jeefb.message code="login.login"/>
										</p>

										<form>
											<fieldset>
												<label class="block clearfix"> <span
													class="block input-icon input-icon-right"> <input
														type="email" class="form-control" placeholder="<@jeefb.message code="common.email"/>" />
														<i class="icon-envelope"></i>
												</span>
												</label> <label class="block clearfix"> <span
													class="block input-icon input-icon-right"> <input
														type="text" class="form-control" placeholder="<@jeefb.message code="common.userName"/>" />
														<i class="icon-user"></i>
												</span>
												</label> <label class="block clearfix"> <span
													class="block input-icon input-icon-right"> <input
														type="password" class="form-control"
														placeholder="<@jeefb.message code="common.password"/>" /> <i class="icon-lock"></i>
												</span>
												</label> <label class="block clearfix"> <span
													class="block input-icon input-icon-right"> <input
														type="password" class="form-control"
														placeholder="<@jeefb.message code="common.repeat.password"/>" /> <i class="icon-retweet"></i>
												</span>
												</label> <label class="block"> <input type="checkbox"
													class="ace" /> <span class="lbl"> <@jeefb.message code="user.accept"/> <a
														href="#">
														<@jeefb.message code="user.agreement"/>
														</a>
												</span>
												</label>

												<div class="space-24"></div>

												<div class="clearfix">
													<button type="reset" class="width-30 pull-left btn btn-sm">
														<i class="icon-refresh"></i> 
														<@jeefb.message code="common.reset"/>
														
													</button>

													<button type="button"
														class="width-65 pull-right btn btn-sm btn-success">
														<@jeefb.message code="user.register"/>
														 <i class="icon-arrow-right icon-on-right"></i>
													</button>
												</div>
											</fieldset>
										</form>
									</div>

									<div class="toolbar center">
										<a href="#" onclick="show_box('login-box'); return false;"
											class="back-to-login-link"> <i class="icon-arrow-left"></i>
											<@jeefb.message code="login.back.login"/>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.main-container -->
	<!-- basic scripts -->
	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery
				|| document
						.write("<script src='${core.themeBase}/ace/js/jquery-2.0.3.min.js'>"
								+ "<"+"/script>");
	</script>
	<!-- <![endif]-->
	<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${core.themeBase}/ace/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
	<script type="text/javascript">
		if ("ontouchend" in document)
			document
					.write("<script src='${core.themeBase}/ace/js/jquery.mobile.custom.min.js'>"
							+ "<"+"/script>");
	</script>
	<script type="text/javascript">
		function show_box(id) {
			jQuery('.widget-box.visible').removeClass('visible');
			jQuery('#' + id).addClass('visible');
		}
	</script>
</body>
</html>