<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><@jeefb.message code="common.platform"/></title>
		<meta name="keywords" content="JAVA开源电商,SpringMVC,Bootstrap,CMS" />
		<meta name="description" content="JAVA开源电商,SpringMVC,Bootstrap,CMS" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="${core.themeBase}/ace/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${core.themeBase}/ace/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="${core.themeBase}/ace/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- ace styles -->
		<link rel="stylesheet" href="${core.themeBase}/ace/css/ace.min.css" />
		<link rel="stylesheet" href="${core.themeBase}/ace/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${core.themeBase}/ace/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="${core.themeBase}/ace/css/ace-ie.min.css" />
		<![endif]-->
		<!-- ace settings handler -->
		<script src="${core.themeBase}/ace/js/ace-extra.min.js"></script>
		<!--[if lt IE 9]>
		<script src="${core.themeBase}/ace/js/html5shiv.js"></script>
		<script src="${core.themeBase}/ace/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		  <#include "top.ftl">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>
			<#include "menu.ftl">
				
					 
					

				<!-- /.main-content -->

				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="icon-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; 选择皮肤</span>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
							<label class="lbl" for="ace-settings-navbar"><@jeefb.message code="meun.navbar"/></label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
							<label class="lbl" for="ace-settings-sidebar"><@jeefb.message code="menu.sidebar"/></label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
							<label class="lbl" for="ace-settings-breadcrumbs"><@jeefb.message code="menu.breadcrumbs"/></label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
							<label class="lbl" for="ace-settings-rtl"><@jeefb.message code="menu.rtl"/></label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
							<label class="lbl" for="ace-settings-add-container">
								<@jeefb.message code="menu.container"/>
								<b></b>
							</label>
						</div>
					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->


		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${core.themeBase}/ace/js/jquery-2.0.3.min.js'>"+"<"+"script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${core.themeBase}/ace/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${core.themeBase}/ace/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
		</script>
		<script src="${core.themeBase}/ace/js/bootstrap.min.js"></script>
		<script src="${core.themeBase}/ace/js/typeahead-bs2.min.js"></script>
		<!--[if lte IE 8]>
		  <script src="${core.themeBase}/ace/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="${core.themeBase}/ace/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="${core.themeBase}/ace/js/jquery.ui.touch-punch.min.js"></script>
		<script src="${core.themeBase}/ace/js/jquery.slimscroll.min.js"></script>
		<script src="${core.themeBase}/ace/js/jquery.easy-pie-chart.min.js"></script>
		<script src="${core.themeBase}/ace/js/jquery.sparkline.min.js"></script>
		<script src="${core.themeBase}/ace/js/flot/jquery.flot.min.js"></script>
		<script src="${core.themeBase}/ace/js/flot/jquery.flot.pie.min.js"></script>
		<script src="${core.themeBase}/ace/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="${core.themeBase}/ace/js/ace-elements.min.js"></script>
		<script src="${core.themeBase}/ace/js/ace.min.js"></script>
</body>
</html>