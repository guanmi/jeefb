<#-- * public html --> 
<#macro html themeBase title>
   <#if themeBase="ace"	>
	<@jeefb.acehtml title="${title}"/>
	<#else>
	</#if>
</#macro>
<#macro acehtml title>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no">
<meta name="msapplication-tap-highlight" content="no">
<title>${title}</title>
<!-- basic styles -->
<link href="${core.themeBase}/ace/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="${core.themeBase}/ace/css/font-awesome.min.css" />
<!--[if IE 7]>
		  <link rel="stylesheet" href="${core.themeBase}/ace/css/font-awesome-ie7.min.css" />
		<![endif]-->
<!-- ace styles -->
<link rel="stylesheet" href="${core.themeBase}/ace/css/ace.min.css" />
<link rel="stylesheet" href="${core.themeBase}/ace/css/ace-rtl.min.css" />
<link rel="stylesheet" href="${core.themeBase}/ace/css/ace-skins.min.css" />
<!--[if lte IE 8]>
		  <link rel="stylesheet" href="${core.themeBase}/ace/css/ace-ie.min.css" />
		<![endif]-->
<!-- ace settings handler -->
<script src="${core.themeBase}/ace/js/ace-extra.min.js"></script>
<!--[if lt IE 9]>
		<script src="${core.themeBase}/ace/js/html5shiv.js"></script>
		<script src="${core.themeBase}/ace/js/respond.min.js"></script>
		<![endif]-->
</head>
<body>
	<#nested/>
	<!-- basic scripts -->
	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery
				|| document
						.write("<script src='${core.themeBase}/ace/js/jquery-2.0.3.min.js'>"
								+ "<"+"script>");
	</script>
	<!-- <![endif]-->
	<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${core.themeBase}/ace/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->
	<script type="text/javascript">
		if ("ontouchend" in document)
			document
					.write("<script src='${core.themeBase}/ace/js/jquery.mobile.custom.min.js'>"
							+ "<"+"script>");
	</script>
	<script src="${core.themeBase}/ace/js/bootstrap.min.js"></script>
	<script src="${core.themeBase}/ace/js/typeahead-bs2.min.js"></script>
	<!-- page specific plugin scripts -->
	<!--[if lte IE 8]>
		  <script src="${core.themeBase}/ace/js/excanvas.min.js"></script>
		<![endif]-->

	<script src="${core.themeBase}/ace/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="${core.themeBase}/ace/js/jquery.ui.touch-punch.min.js"></script>
	<script src="${core.themeBase}/ace/js/jquery.slimscroll.min.js"></script>
	<script src="${core.themeBase}/ace/js/jquery.easy-pie-chart.min.js"></script>
	<script src="${core.themeBase}/ace/js/jquery.sparkline.min.js"></script>
	<script src="${core.themeBase}/ace/js/flot/jquery.flot.min.js"></script>
	<script src="${core.themeBase}/ace/js/flot/jquery.flot.pie.min.js"></script>
	<script src="${core.themeBase}/ace/js/flot/jquery.flot.resize.min.js"></script>
	<!-- ace scripts -->
	<script src="${core.themeBase}/ace/js/ace-elements.min.js"></script>
	<script src="${core.themeBase}/ace/js/ace.min.js"></script>
</body>
</html>
</#macro>
<#--
 * message
 *
 * Macro to translate a message code into a message.
 -->
<#macro message code>${springMacroRequestContext.getMessage(code)}</#macro>