<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录 - JEE FAST BOOT</title>
    <link href="${core.themeBase}/nifty/css/bootstrap.min.css" rel="stylesheet">
    <link href="${core.themeBase}/nifty/css/nifty.min.css" rel="stylesheet">
    <link href="${core.themeBase}/nifty/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="${core.themeBase}/nifty/css/jeefb/nifty-jeefb.min.css" rel="stylesheet">
    <link href="${core.themeBase}/nifty/plugins/pace/pace.min.css" rel="stylesheet">
    <script src="${core.themeBase}/nifty/plugins/pace/pace.min.js"></script>
</head>
<body>
	<div id="container" class="cls-container">
		<div id="bg-overlay" class="bg-img img-balloon"></div>
		<div class="cls-header cls-header-lg">
			<div class="cls-brand">
				<a class="box-inline" href="${core.themeBase}/nifty/index.html">
					<span class="brand-title"><@jeefb.message code="common.platform"/></span>
				</a>
			</div>
		</div>
		<div class="cls-content">
			<div class="cls-content-sm panel">
				<div class="panel-body">
					<p class="pad-btm"><@jeefb.message code="login.please"/></p>
					<form action="${core.base}/login" method="post">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-user"></i></div>
								<input type="text" class="form-control" placeholder="<@jeefb.message code="common.userName"/>">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
								<input type="password" class="form-control" placeholder="<@jeefb.message code="common.password"/>">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-8 ">
								<input type="password" class="form-control" placeholder="<@jeefb.message code="login.verfiyCode"/>">
							</div>
							<div class="col-xs-4">
								<div class="form-group text-right">
								<img  src="${core.base}/servlet/validateCodeServlet" class="width-35 pull-right "/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-8 text-left checkbox">
								<label class="form-checkbox form-icon">
								<input type="checkbox"> <@jeefb.message code="login.remember"/>
								</label>
							</div>
							<div class="col-xs-4">
								<div class="form-group text-right">
								<button class="btn btn-success text-uppercase" type="submit"><@jeefb.message code="login.login"/></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="pad-ver">
				<a href="${core.themeBase}/nifty/pages-password-reminder.html" class="btn-link mar-rgt"><@jeefb.message code="login.forgot.password"/></a>
				<a href="${core.themeBase}/nifty/pages-register.html" class="btn-link mar-lft"><@jeefb.message code="login.register"/></a>
			</div>
		</div>
		<div class="jeefb-bg">
			<div id="jeefb-bg-list">
				<div class="jeefb-loading"><i class="fa fa-refresh"></i></div>
				<img class="jeefb-chg-bg bg-trans" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-trns.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-1.jpg" alt="Background Image">
				<img class="jeefb-chg-bg active" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-2.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-3.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-4.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-5.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-6.jpg" alt="Background Image">
				<img class="jeefb-chg-bg" src="${core.themeBase}/nifty/img/bg-img/thumbs/bg-img-7.jpg" alt="Background Image">
			</div>
		</div>
	</div>
    <script src="${core.themeBase}/nifty/js/jquery-2.1.1.min.js"></script>
    <script src="${core.themeBase}/nifty/js/bootstrap.min.js"></script>
    <script src="${core.themeBase}/nifty/plugins/fast-click/fastclick.min.js"></script>
    <script src="${core.themeBase}/nifty/js/nifty.min.js"></script>
    <script src="${core.themeBase}/nifty/js/jeefb/bg-images.js"></script>
</body>
</html>
